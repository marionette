import os, re

opts = Options('scache.conf')
### These options are global defaults.
### If you want to change your options locally,
### give them to SCons on the command line, or
### edit scache.conf. Don't edit them here.
opts.AddOptions(
    ('CC', 'Set the C compiler to use'),
    ('AS', 'Set the assembler to use'),
    ('LINK', 'Set the linker to use'),
    ('CFLAGS', 'Set the C compiler flags', '-ffreestanding -m32 -O3 -Wall -std=c99 -pedantic'),
    ('ASFLAGS', 'Set the assembler flags', '-m32'),
    ('LINKFLAGS', 'Set the linker flags', '-m32 -ffreestanding -nostdlib -Wl,--gc-sections,-m,elf_i386'),
    ('BUILDDIR', 'Set the sub-directory to put object files in', 'build'),
    BoolOption('verbose', 'Show full commands during the build process', False),
)

env = Environment(
    options = opts,
    tools=['default'],
    ENV = os.environ)
Help(opts.GenerateHelpText(env))
opts.Save('scache.conf', env)

##### CONFIGURATION #####

class OurTests:
    def check_stack_prot(context):
        """check whether gcc supports -fno-stack-protector"""
        context.Message('Checking for -fno-stack-protector... ')
        context.env['CFLAGS'] = '-fno-stack-protector'
        result = context.TryCompile('', '.c')
        context.Result(result)
        return result

    def check_for_asciidoc(context):
        """check whether asciidoc is installed"""
        context.Message('Checking for asciidoc... ')
        result = context.TryAction(Action('asciidoc $SOURCE'), '', '.txt')
        context.Result(result[0])
        return result[0]

old_env = env.Clone()

env.build_doc = True

if not env.GetOption('clean'):
    ### RUN TESTS AND CONFIGURE ENVIRONMENT ###
    conf = Configure(env, custom_tests = OurTests.__dict__)
    
    # If the compiler has -fno-stack-protector, we need to disable
    # it to stop gcc inserting stack protection code (which doesn't
    # work in a kernel)
    if conf.check_stack_prot():
        env['CFLAGS'] = old_env['CFLAGS'] + ' -fno-stack-protector'
    else:
        env['CFLAGS'] = old_env['CFLAGS']

    if not conf.check_for_asciidoc():
        env.build_doc = False

    env = conf.Finish()

##### SETTINGS #####

import tools.configure

env.conf = tools.configure.Config('CONFIG', os.path.join(env['BUILDDIR'], 'config'))

##### BUILD RULES #####

# link a kernel module

def KernelModule(env, target, sources, config_name = None):
    if config_name:
        build_type = env.conf.get(config_name)
    else:
        build_type = 'm'
    if build_type == 'y':
        # nothing special - compile into kernel like all the other kernel objects
        env.kernel_objects.extend(env.Object(i) for i in sources)
    elif build_type == 'm':
        # build as a module
        # NOTE: module_entry_point is set in SConscript
        module = env.Program('%s.ko' % target[0], sources + [env.module_entry_point],
            LINKFLAGS = env['LINKFLAGS'] + ' -Wl,-r,-T,' + env.module_linker_script.path,
            CCCOMSTR = '$MODCCCOMSTR',
            LINKCOMSTR = '$MODLINKCOMSTR',
        )
        env.Depends(module, env.module_linker_script)
    else:
        # don't build at all!
        pass

# build asciidoc documentation
AsciiDoc = env.Builder(
    action = Action('asciidoc -o $TARGET $SOURCE', '$DOCCOMSTR'),
    suffix = '.html',
    src_suffix = '.txt')

env.Append(BUILDERS = {'KernelModule': KernelModule,
                       'AsciiDoc': AsciiDoc})
env.module_linker_script = File('kernel/module.ld')

##### VANITY #####

# set nice pretty messages
if not env['verbose']:
    env['CCCOMSTR'] =      '     Compiling [32m$TARGET[0m'
    env['ASPPCOMSTR'] =    '    Assembling [32m$TARGET[0m'
    env['LINKCOMSTR'] =    '       Linking [32m$TARGET[0m'
    env['MODCCCOMSTR'] =   '[M]  Compiling [32m$TARGET[0m'
    env['MODLINKCOMSTR'] = '[M]    Linking [32m$TARGET[0m'
    env['ARCOMSTR'] =      '     Archiving [32m$TARGET[0m'
    env['RANLIBCOMSTR'] =  '      Indexing [32m$TARGET[0m'
    env['NMCOMSTR'] =      '  Creating map [32m$TARGET[0m'
    env['DOCCOMSTR'] =     '   Documenting [32m$TARGET[0m'

##### SUB-DIRECTORIES #####

SConscript('SConscript',
    exports = ['env'],
    build_dir = env['BUILDDIR'],
    duplicate = 0)

# clean config data
env.Clean('.', ['config.log', '.sconf_temp'])
