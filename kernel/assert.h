#ifndef ASSERT_H
#define ASSERT_H

#include "panic.h"

#define assert(expr) do { \
    if (!(expr)){ \
        panic("Assertion failed: \"%s\"", #expr); \
    } \
} while (0)

#endif
