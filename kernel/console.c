/*
 *    Copyright (c) 2008 Joshua Phillips. All rights reserved.
 *  
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are
 *  met:
 *  
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in
 *       the documentation and/or other materials provided with the
 *       distribution.
 *  
 *  THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 *  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 *  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 *  IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 *  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 *  IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "console.h"
#include "portio.h"
#include "string.h"
#include "stdio.h"

struct cdev_tty {
    struct cdev cdev;
};

static int tty_write(struct cdev *cdev, void *ptr, size_t count);

static const struct cdev_ops tty_ops = {
    tty_write,
};

struct cdev_tty _dev_tty1 = {
    { &tty_ops, },
};

struct cdev *const dev_tty1 = &_dev_tty1.cdev;

static int tty_write(struct cdev *cdev, void *ptr, size_t count)
{
    char *c_ptr = ptr;
    int i;
    for (i=0; i<count; ++i){
        console_putchar(&tty1, *c_ptr++);
    }
    return count;
}

struct console tty1;

// get position of the hardware cursor
static void get_hw_cursor(struct console *self)
{
    unsigned int tmp;
    outb(0x3D4, 14);
    tmp = inb(0x3D5) << 8;
    outb(0x3D4, 15);
    tmp |= inb(0x3D5);
    self->y = tmp / self->width;
    self->x = tmp % self->width;
}

// set position of the hardware cursor
static void set_hw_cursor(struct console *self, int x, int y)
{
    unsigned int tmp;
    tmp = y * self->width + x;
    outb(0x3D4, 14);
    outb(0x3D5, tmp >> 8);
    outb(0x3D4, 15);
    outb(0x3D5, tmp);
}

// scroll up n lines
static void scroll(struct console *self, int n)
{
    unsigned short p;
    // Move everything up
    memmove(self->vram,
        self->vram + n * self->width,
        self->width * (self->height - n) * sizeof *self->vram);
    // Clear the bottom
    for (p = self->width * (self->height - n)
    ; p < (self->height * self->width); p++){
        *(self->vram + p) = (self->attrib << 8) | ' ';
    }
    self->y -= n;
}

// process a newline
static void newline(struct console *self)
{
    self->x = 0;
    self->y++;
    if (self->y >= self->height){
        scroll(self, self->y + 1 - self->height);
    }
}

// process a backspace
static void backspace(struct console *self)
{
    if (self->x == 0)
        return;
    self->x--;
    self->vram[self->y * self->width + self->x] = (self->attrib << 8) | ' ';
}

// handle special characters
static void special_chars(struct console *self, char ch)
{
    if (ch == '\n'){
        newline(self);
    } else if (ch == '\b'){
        backspace(self);
    } else {
        console_printf(self, "\n*** TODO: Implement character %.8X\n", ch);
    }
}

void console_init(struct console *self)
{
    self->vram = (unsigned short *) 0x000B8000;
    self->width = 80;
    self->height = 25;
    get_hw_cursor(self);
    self->attrib = 0x07; // white on black
}

void console_clear(struct console *self)
{
    int i;
    for (i=0; i<(self->width * self->height); i++)
        self->vram[i] = (self->attrib << 8) | ' ';
}

void console_colour_default(struct console *self)
{
    self->attrib = 0x07;
}

void console_colour_fg(struct console *self, int fg)
{
    self->attrib = (self->attrib & ~0xF) | fg;
}

void console_colour_fgbg(struct console *self, int fg, int bg)
{
    self->attrib = (bg << 4) | fg;
}

void console_putchar(struct console *self, char ch)
{
    if (ch < ' ' || ch == 0x7F){
        special_chars(self, ch);
    } else {
        if (self->y >= self->height){
            scroll(self, self->y + 1 - self->height);
        }
        self->vram[self->y * self->width + self->x] = (self->attrib << 8) | ch;
        self->x++;
        if (self->x == self->width){
            newline(self);
        }
    }
}

void console_puts(struct console *self, const char *str)
{
    while (*str){
        console_putchar(self, *str);
        str++;
    }
    set_hw_cursor(self, self->x, self->y);
}

void console_printf(struct console *self, const char *fmt, ...)
{
    va_list ap;
    va_start(ap, fmt);
    console_vprintf(self, fmt, ap);
    va_end(ap);
}

void console_vprintf(struct console *self, const char *fmt, va_list ap)
{
    char buf[256]; // should be enough for anyone :)
    vsnprintf(buf, 255, fmt, ap);
    console_puts(self, buf);
}
