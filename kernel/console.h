/*
 *    Copyright (c) 2008 Joshua Phillips. All rights reserved.
 *  
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are
 *  met:
 *  
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in
 *       the documentation and/or other materials provided with the
 *       distribution.
 *  
 *  THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 *  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 *  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 *  IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 *  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 *  IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef CONSOLE_H
#define CONSOLE_H

#include "stdarg.h"
#include "chardev.h"

extern struct cdev *const dev_tty1;

// Terminal colours

enum {
    COLOUR_BLACK = 0,
    COLOUR_BLUE,
    COLOUR_GREEN,
    COLOUR_CYAN,
    COLOUR_RED,
    COLOUR_MAGENTA,
    COLOUR_BROWN,
    COLOUR_WHITE,
    COLOUR_GREY,
    COLOUR_BRIGHT_BLUE,
    COLOUR_BRIGHT_GREEN,
    COLOUR_BRIGHT_CYAN,
    COLOUR_BRIGHT_RED,
    COLOUR_BRIGHT_MAGENTA,
    COLOUR_BRIGHT_YELLOW,
    COLOUR_BRIGHT_WHITE
};

struct console {
    unsigned short *vram; // pointer to vram
    int x, y; // cursor co-ordinates
    int width, height; // screen size, in characters
    int attrib; // current attributes (colour, etc.)
};

void console_init(struct console *self); // set up the console, ready for use
void console_clear(struct console *self); // clear the contents of the console
void console_colour_default(struct console *self); // Reset to the default colour
void console_colour_fg(struct console *self, int fg); // Set foreground colour - retain background colour
void console_colour_fgbg(struct console *self, int fg, int bg); // Set foreground and background colour
void console_putchar(struct console *self, char ch); // Write a single character to the console
void console_puts(struct console *self, const char *str); // Write 'str' to the console
void console_printf(struct console *self, const char *fmt, ...); // Write 'str' to the console, with formatting
void console_vprintf(struct console *self, const char *fmt, va_list ap); // Write 'str' to the console, with formatting
void console_move_cursor(int dx, int dy);

extern struct console tty1; // default console

#endif

