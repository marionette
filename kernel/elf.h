// This file was created based on information from:
// http://x86.ddj.com/ftp/manuals/tools/elf.pdf

#ifndef ELF_H
#define ELF_H

typedef unsigned short Elf32_Half;
typedef unsigned long Elf32_Word;
typedef signed long Elf32_Sword;
typedef unsigned long Elf32_Addr;
typedef unsigned long Elf32_Off;

// Elf32_Ehdr::e_ident indices
enum {
	EI_MAG0 = 0,   // file identification
	EI_MAG1,       // file identification
	EI_MAG2,       // file identification
	EI_MAG3,       // file identification
	EI_CLASS,      // file class
	EI_DATA,       // data encoding
    EI_VERSION,    // file version
    EI_PAD,        // start of padding bytes
    EI_NIDENT = 16 // number of entries in e_ident[]
};

// magic bytes (this is really overdoing the use-enums-instead-of-macros philosophy)
enum {
    ELFMAG0 = 0x7F,
    ELFMAG1 = 'E',
    ELFMAG2 = 'L',
    ELFMAG3 = 'F',
};

// EI_CLASS
enum {
    ELFCLASSNONE = 0, // invalid class
    ELFCLASS32,       // 32-bit objects
    ELFCLASS64,       // 64-bit objects
};

// EI_DATA (data encoding)
enum {
    ELFDATANONE = 0, // invalid data encoding
    ELFDATA2LSB,     // little-endian
    ELFDATA2MSB,     // big-endian
};

// Elf32_Ehdr::e_type
enum {
    ET_NONE = 0,        // no file type
    ET_REL,             // relocatable file
    ET_EXEC,            // executable file
    ET_DYN,             // shared object file
    ET_CORE,            // core file
    ET_LOPROC = 0xFF00, // processor-specific
    ET_HIPROC = 0xFFFF, // processor-specific
};

// Elf32_Ehdr::e_machine
enum {
    EM_NONE = 0,         // no machine
    EM_M32,              // AT&T WE 32100
    EM_SPARC,            // SPARC
    EM_386,              // Intel Architecture
    EM_68K,              // Motorola 68000
    EM_88K,              // Motorola 88000
    EM_860 = 7,          // Intel 80860
    EM_MIPS,             // MIPS RS3000 Big-Endian
    EM_MIPS_RS4_RE = 10, // MIPS RS4000 Big-Endian
    // 11-16 reserved
};

// Elf32_Ehdr::e_version
enum {
    EV_NONE = 0,    // invalid version
    EV_CURRENT = 1, // current version
};

struct Elf32_Ehdr {
    unsigned char e_ident[EI_NIDENT];
    Elf32_Half e_type;        // ELF file type
    Elf32_Half e_machine;     // machine
    Elf32_Word e_version;     // object file version
    Elf32_Addr e_entry;       // virtual address of entry point
    Elf32_Off e_phoff;        // file offset to program header table
    Elf32_Off e_shoff;        // file offset to section header table
    Elf32_Word e_flags;       // processor-specific flags
    Elf32_Half e_ehsize;      // ELF file header size
    Elf32_Half e_phentsize;   // Program header size
    Elf32_Half e_phnum;       // number of program headers
    Elf32_Half e_shentsize;   // Section header size
    Elf32_Half e_shnum;       // Number of section headers (number of sections)
    Elf32_Half e_shstrndx;    // String table for section names (usually ".shstrtab")
};

// Special section indices
enum {
    SHN_UNDEF = 0, // undefined/missing/irrelevant/meaningless section reference
	SHN_LORESERVE = 0xFF00, // lower bound of the range of reserved indices
	SHN_LOPROC    = 0xFF00, // processor-specific
	SHN_HIPROC    = 0xFF1F, // processor-specific
	SHN_ABS       = 0xFFF1, // absolute symbol value (not section-relative)
	SHN_COMMON    = 0xFFF2, // common symbol (eg. unallocated C external variables)
	SHN_HIRESERVE = 0xFFFF, // upper bound of the range of reserved indices
};

// sh_type
enum {
	SHT_NULL = 0, // inactive section header. All other section header fields are invalid.
	SHT_PROGBITS, // data defined by the program: code or data
	SHT_SYMTAB,   // symbol table
	SHT_STRTAB,   // string table
	SHT_RELA,     // relocations with explicit addends
	SHT_HASH,     // symbol hash table
	SHT_DYNAMIC,  // information for dynamic linking
	SHT_NOTE,     // information that marks the file in some way
	SHT_NOBITS,   // section occupies no space in the file but otherwise resembles SHT_PROGBITS.
	SHT_REL,      // relocation entries without explicit addends
	SHT_SHLIB,    // reserved; unspecified semantics
	SHT_DYNSYM,   // symbol table (dynamic symbols)

    SHT_LOPROC = 0x70000000,    // ridiculous range of processor-specific values
    SHT_HIPROC = 0x7FFFFFFF,
    SHT_LOUSER = (int) 0x80000000,    // ridiculous range of values available to application programs
    SHT_HIUSER = (int) 0xFFFFFFFF,
};

/* sh_type         sh_link                  sh_info
   --------------------------------------------------------------
   SHT_DYNAMIC     string table             SHN_UNDEF
   --------------------------------------------------------------
   SHT_HASH        symbol table to which
                   hash table applies
   --------------------------------------------------------------
   SHT_REL         section number of        section to which the
   SHT_RELA        associated symbol table  relocations apply
   --------------------------------------------------------------
   SHT_SYMTAB      operating system         operating system
                   specific                 specific
                   (SysV - section number   (SysV - index of first
                   of associated string     global symbol)
                   table)
   --------------------------------------------------------------
	other          SHN_UNDEF                SHN_UNDEF
*/

// Section header
struct Elf32_Shdr {
	Elf32_Word sh_name; // section name (index into e_shstrndx section)
	Elf32_Word sh_type; // categorizes section's contents and semantics (see above)
	Elf32_Word sh_flags; // miscellaneous (see above)
	Elf32_Addr sh_addr; // section virtual address
	Elf32_Off sh_offset; // file offset to section data
	Elf32_Word sh_size; // section size, in bytes
	Elf32_Word sh_link; // depends on sh_type
	Elf32_Word sh_info; // depends on sh_type
	Elf32_Word sh_addralign; // address alignment constraint
	Elf32_Word sh_entsize; // if section contains fixed-size entries, this is the size of one entry. Otherwise, zero.
};

// Special symbol indices
enum {
	STN_UNDEF = 0,
};

// Symbol type/binding
static inline unsigned int ELF32_ST_BIND(unsigned char info)
{
    return info >> 4;
}

static inline unsigned int ELF32_ST_TYPE(unsigned char info)
{
    return info & 0xF;
}

static inline unsigned char ELF32_ST_INFO(unsigned int bind, unsigned int type)
{
    return (bind << 4) | (type & 0xF);
}

enum {
	// binding
	STB_LOCAL = 0,   // local symbols ('static', in C)
	STB_GLOBAL,      // global symbols
	STB_WEAK,        // weak symbols (??? in C)
	STB_LOPROC = 13, // processor-specific
	STB_HIPROC = 15, // processor-specific
};
enum {
	// type
	STT_NOTYPE = 0, // type not specified
	STT_OBJECT,     // data object - variable, etc.
	STT_FUNC,       // function or other executable code
	STT_SECTION,    // associated with a section (primarily for relocation)
	STT_FILE,       // a file symbol has STB_LOCAL binding, its section index
	                // is SHN_ABS, and it precedes the other STB_LOCAL symbols for the file,
	                // if it is present.
};

// Symbol
struct Elf32_Sym {
	Elf32_Word st_name;     // symbol name (index into symbol string table)
	Elf32_Addr st_value;    // value of associated symbol (section offset or absolute address)
	Elf32_Word st_size;     // size of symbol, usually zero
	unsigned char st_info;  // type and binding (see ELF32_ST_xxx)
	unsigned char st_other; // zero (no defined meaning)
	Elf32_Half st_shndx;    // section to which symbol is relative (SHN_ABS -> st_value is absolute)
};

// Relocation entry
static inline unsigned int ELF32_R_SYM(Elf32_Word info)
{
    return info >> 8;
}

static inline unsigned int ELF32_R_TYPE(Elf32_Word info)
{
    return (unsigned char) info;
}

static inline Elf32_Word ELF32_R_INFO(unsigned int sym, unsigned int type)
{
    return (sym << 8) | ((unsigned char) type);
}

enum {
	// relocation types
	R_386_NONE = 0, // no relocation
	R_386_32,       // symbol + addend
	R_386_PC32,     // symbol + addend - (location+4)
};

struct Elf32_Rel {
	Elf32_Addr r_offset;  // location at which to apply the relocation action.
	                      // Relocatable file (ET_REL) - this is a section offset
	                      // Executable or shared object (ET_EXEC, ET_DYN) - virtual address
	Elf32_Word r_info;    // relocation info - symbol index and relocation type (see ELF32_R_XXX)
};

struct Elf32_Rela {
	Elf32_Addr r_offset;  // same as Elf32_Rel
	Elf32_Word r_info;    // same as Elf32_Rel
	Elf32_Sword r_addend; // explicit addend to add to symbol value
} Elf32_Rela;

// Segment types
enum {
	PT_NULL = 0, // entry is ignored
	PT_LOAD,     // loadable segment - segment is part of the program
	PT_DYNAMIC,  // dynamic linking information
	PT_INTERP,   // location and size of a null-terminated path name to invoke as an interpreter
	PT_NOTE,     // location and size of auxiliary information
	PT_SHLIB,    // reserved; unspecified semantics
	PT_PHDR,     // if present, specifies location and size of program header
	             // so you can load the program header into memory!

    PT_LOPROC = 0x70000000,
    PT_HIPROC = 0x7FFFFFFF,
};

// Segment flags
enum {
    PF_X = 0x01,    // executable
    PF_W = 0x02,    // writable
    PF_R = 0x04,    // readable
    PF_MASKPROC = (int) 0xF0000000,
};

// Program Header
struct Elf32_Phdr {
	Elf32_Word p_type;   // segment type
	Elf32_Off p_offset;  // file offset of segment
	Elf32_Addr p_vaddr;  // virtual address to load to
	Elf32_Addr p_paddr;  // segment's physical address (relevant in a kernel, for example)
	Elf32_Word p_filesz; // number of bytes occupied in file (may be zero)
	Elf32_Word p_memsz;  // number of bytes occupied in memory (may be zero)
	Elf32_Word p_flags;  // flags
	Elf32_Word p_align;  // segment alignment (zero, or power of two)
};

#endif
