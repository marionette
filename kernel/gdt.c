/*
 *    Copyright (c) 2008 Joshua Phillips. All rights reserved.
 *  
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are
 *  met:
 *  
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in
 *       the documentation and/or other materials provided with the
 *       distribution.
 *  
 *  THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 *  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 *  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 *  IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 *  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 *  IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "gdt.h"
#include "stddef.h"
#include "string.h"

extern char stack[];

// The Global Descriptor Table (it is copied to a different location later)
// We only need a few hard-coded entries in this.
static unsigned char gdt_base[] = {
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // dummy (the CPU doesn't use gdt[0])
    0xFF, 0xFF, 0x00, 0x00, 0x00, 0x9A, 0xCF, 0x00, // Code segment, DPL=0, 0x00000000..0xFFFFFFFF - SEG_DPL0_CODE
    0xFF, 0xFF, 0x00, 0x00, 0x00, 0x92, 0xCF, 0x00, // Data segment, DPL=0, 0x00000000..0xFFFFFFFF - SEG_DPL0_DATA
    0xFF, 0xFF, 0x00, 0x00, 0x00, 0xFA, 0xCF, 0x00, // Code segment, DPL=3, 0x00000000..0xFFFFFFFF - SEG_DPL3_CODE
    0xFF, 0xFF, 0x00, 0x00, 0x00, 0xF2, 0xCF, 0x00, // Data segment, DPL=3, 0x00000000..0xFFFFFFFF - SEG_DPL3_DATA
    0x68, 0x00, 0xAA, 0xAA, 0xAA, 0x89, 0x00, 0xAA, // TSS descriptor (address is initialized to 0xAAAAAAAA,
                                                    // so it's easier to see if someting goes wrong)
};

static struct tss *tss0 = NULL; // The default TSS. We'll need one for every CPU core.

// load task register with a task state segment descriptor
static void load_tss(int tss_seg_sel)
{
    asm volatile("ltr %%ax" :: "a" (tss_seg_sel));
}

// load data-segment registers
static void load_sreg(int data_seg_sel)
{
    asm volatile (
      "movw %%ax,%%ds \n"
      "movw %%ax,%%es \n"
      "movw %%ax,%%fs \n"
      "movw %%ax,%%gs \n"
      :: "a" (data_seg_sel));
}

// give the CPU the address of our GDT
static void load_gdt(void *gdt_address, size_t gdt_size)
{
    asm volatile (
      // put the address and size of the GDT on the stack,
      // in the format that the lgdt instruction wants it.
      "subl $6,%%esp        \n"
      "movw %%cx,(%%esp)    \n"
      "movl %%eax,2(%%esp)  \n"
      "lgdt (%%esp)         \n"
      "addl $6,%%esp        \n"
      :: "c" (gdt_size), "a" (gdt_address)
      : "cc");
}

// Main initialization function for the GDT
void gdt_init(void **p_gdt)
{
    uint8_t *gdt;

    // Give enough space for the GDT
    gdt = (uint8_t *) *p_gdt;
    *p_gdt = gdt + sizeof gdt_base;

    // Copy the gdt_base array into our real GDT
    memcpy(gdt, gdt_base, sizeof gdt_base);

    // Give enough space for the TSS
    tss0 = (struct tss *) *p_gdt;
    *p_gdt = tss0 + 1;

    // Set up the task state segment decriptor
    uint32_t tss_addr = (uint32_t) tss0;
    gdt[0x2A] = tss_addr;
    gdt[0x2B] = tss_addr >> 8;
    gdt[0x2C] = tss_addr >> 16;
    gdt[0x2F] = tss_addr >> 24;

    // Set the essential TSS fields
    tss0->ss0 = SEG_DPL0_DATA;
    tss0->esp0 = (uint32_t) (stack + 0x4000); // XXX: don't hard-code

    // Tell the CPU where to find the GDT
    load_gdt(gdt, sizeof gdt_base);
    
    // Re-load all the data segment registers.
    // We can use the ring 3 segments, then we don't have to keep
    // switching. This is not a problem, because we're not using
    // segmentation as a means of protection.
    load_sreg(SEG_DPL3_DATA);

    // Load the task register
    load_tss(SEG_DPL3_TSS);
}
