/*
 *    Copyright (c) 2008 Joshua Phillips. All rights reserved.
 *  
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are
 *  met:
 *  
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in
 *       the documentation and/or other materials provided with the
 *       distribution.
 *  
 *  THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 *  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 *  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 *  IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 *  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 *  IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


#ifndef GDT_H
#define GDT_H

#include "stdint.h"

// Segment selectors - these correspond to entries
// in the GDT (in gdt.c)
#define SEG_DPL0_CODE 0x08 // Kernel-level code
#define SEG_DPL0_DATA 0x10 // Kernel-level data
#define SEG_DPL3_CODE 0x1B // User-level code
#define SEG_DPL3_DATA 0x23 // User-level data
#define SEG_DPL3_TSS  0x2B // User-level TSS

// The structure of the Task State Segment
struct tss {
    uint16_t prev, res0;
    uint32_t esp0;
    uint16_t ss0, res1;
    uint32_t esp1;
    uint16_t ss1, res2;
    uint32_t esp2;
    uint16_t ss2, res3;
    uint32_t cr3, eip, eflags, eax, ecx, edx, ebx, esp, ebp, esi, edi;
    uint16_t es, res4, cs, res5, ss, res6, ds, res7, fs, res8, gs, res9;
    uint16_t ldt, res10, debug_trap, iomap_base;
};

// Main initialization function for the GDT
void gdt_init(void **p_gdt);

#endif
