/*
 *    Copyright (c) 2008 Joshua Phillips. All rights reserved.
 *  
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are
 *  met:
 *  
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in
 *       the documentation and/or other materials provided with the
 *       distribution.
 *  
 *  THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 *  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 *  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 *  IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 *  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 *  IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "interrupt.h"
#include "gdt.h"
#include "console.h"
#include "portio.h"

/* PIC registers */
#define PIC1_CMD 0x20
#define PIC1_DATA 0x21
#define PIC2_CMD 0xA0
#define PIC2_DATA 0xA1

/* PIC commands */
#define PIC_ACK 0x20
#define ICW1_ICW4 0x01
#define ICW1_SINGLE 0x02
#define ICW1_INTERVAL4 0x04
#define ICW1_LEVEL 0x08
#define ICW1_INIT 0x10
#define ICW4_8086 0x01
#define ICW4_AUTO 0x02
#define ICW4_BUF_SLAVE 0x08
#define ICW4_BUF_MASTER 0x0C
#define ICW4_SFNM 0x10

isr_t interrupt_handlers[256];
static uint16_t *idt = NULL;

void isr0(void);
void isr1(void);

static void default_interrupt_handler(int vector, struct interrupt_stack *is)
{
	console_printf(&tty1, "Interrupt 0x%X fired (not handled) (eip=%.8X).\n", vector, is->eip);
	if (vector >= 0x20 && vector < 0x30)
		ack_irq(vector - 0x20);
}

static void load_idt_addr(unsigned short *idt)
{
	asm volatile (
	  "subl $6,%%esp        \n"
	  "movw %%cx,(%%esp)    \n"
	  "movl %%eax,2(%%esp)  \n"
	  "lidt (%%esp)         \n"
	  "addl $6,%%esp        \n"
	  :: "c" (256 * 8), "a" (idt)
	  : "cc");
}

static void io_wait(void)
{
	int i;
	for (i=0; i<16; i++)
		outb(0x80, 0x00);
}

static void remap_pics(void)
{
	int pic1_offset = 0x20;
	int pic2_offset = 0x28;
	unsigned char a1, a2;

	//a1 = inb(PIC1_DATA);
	//a2 = inb(PIC2_DATA);
	a1 = 0xFF;
	a2 = 0xFF;

	outb(PIC1_CMD, ICW1_INIT + ICW1_ICW4);
	io_wait();
	outb(PIC2_CMD, ICW1_INIT + ICW1_ICW4);
	io_wait();
	outb(PIC1_DATA, pic1_offset);
	io_wait();
	outb(PIC2_DATA, pic2_offset);
	io_wait();
	outb(PIC1_DATA, 4);
	io_wait();
	outb(PIC2_DATA, 2);
	io_wait();
	outb(PIC1_DATA, ICW4_8086);
	io_wait();
	outb(PIC2_DATA, ICW4_8086);
	io_wait();
	outb(PIC1_DATA, a1);
	outb(PIC2_DATA, a2);
}

void interrupt_init(void **p_idt)
{
	int i, isr_size;
	unsigned long p;
    idt = (uint16_t *) *p_idt;
    *p_idt = idt + (256 * 4);

	isr_size = (unsigned long) isr1 - (unsigned long) isr0;
	p = (unsigned long) isr0;

	// fill in the IDT
	for (i=0; i<256; i++){
		idt[i * 4 + 0] = p;
		idt[i * 4 + 1] = SEG_DPL0_CODE;
		idt[i * 4 + 2] = 0x8E00;
		idt[i * 4 + 3] = p >> 16;
		p += isr_size;

		// set the default interrupt handler, too
		interrupt_handlers[i] = default_interrupt_handler;
	}

	// load the IDT address
	load_idt_addr(idt);

	// remap the PICs
	remap_pics();

	// Enable the cascade irq (so that the slave PIC can work)
	unmask_irq(2);
}

void enable_interrupts(void)
{
	asm volatile ("sti");
}

void disable_interrupts(void)
{
	asm volatile ("cli");
}

void set_interrupt(int vector, isr_t handler)
{
	interrupt_handlers[vector] = handler;
}

void clear_interrupt(int vector)
{
	interrupt_handlers[vector] = default_interrupt_handler;
}

void interrupt_flags(int vector, unsigned int flags)
{
    if (flags & INTR_USER){
        idt[vector * 4 + 2] = 0xEE00;
    } else {
        idt[vector * 4 + 2] = 0x8E00;
    }
}

int irq_to_int(int irq)
{
	return irq + 0x20;
}

int int_to_irq(int intn)
{
	return intn - 0x20;
}

void unmask_irq(int n)
{
	unsigned char a;
	if (n < 8){
		// master PIC
		a = inb(PIC1_DATA);
		a &= ~(1 << n);
		outb(PIC1_DATA, a);
	} else {
		// slave PIC
		a = inb(PIC2_DATA);
		a &= ~(1 << (n - 8));
		outb(PIC2_DATA, a);
	}
}

void mask_irq(int n)
{
	unsigned char a;
	if (n < 8){
		// master PIC
		a = inb(PIC1_DATA);
		a |= (1 << n);
		outb(PIC1_DATA, a);
	} else {
		// slave PIC
		a = inb(PIC2_DATA);
		a |= (1 << (n - 8));
		outb(PIC2_DATA, a);
	}
}

void ack_irq(int n)
{
	if (n >= 8)
		outb(PIC2_CMD, PIC_ACK);
	outb(PIC1_CMD, PIC_ACK);
}

