/*
 *    Copyright (c) 2008 Joshua Phillips. All rights reserved.
 *  
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are
 *  met:
 *  
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in
 *       the documentation and/or other materials provided with the
 *       distribution.
 *  
 *  THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 *  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 *  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 *  IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 *  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 *  IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef INTERRUPT_H
#define INTERRUPT_H

#include "stdint.h"
#include "cpustruct.h"

// This is the stack structure that arises from an interrupt
struct interrupt_stack {
    struct pusha_struct r;
    uint32_t error_code, eip, cs, eflags, esp, ss;
};

// Interrupt flags (do not correspond to hardware flags)
enum {
    INTR_USER = 0x01,   //user can invoke swi
};

typedef void (* isr_t)(int vector, struct interrupt_stack *);
extern isr_t interrupt_handlers[];

void interrupt_init(void **p_idt);
void enable_interrupts(void); // enable interrupts globally
void disable_interrupts(void); // disable interrupts globally
void set_interrupt(int vector, isr_t handler); // set up an interrupt handler
void clear_interrupt(int vector); // remove an interrupt handler
void interrupt_flags(int vector, unsigned int flags); // set interrupt flags (INTR_*)

int irq_to_int(int irq); // get an interrupt vector of an irq
int int_to_irq(int intn); // get the irq number behind an interrupt vector
void unmask_irq(int n); // unmask (enable) IRQ n
void mask_irq(int n); // mask (disable) IRQ n
void ack_irq(int n); // acknowledge IRQ n

#endif
