/*
 *    Copyright (c) 2008 Joshua Phillips. All rights reserved.
 *  
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are
 *  met:
 *  
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in
 *       the documentation and/or other materials provided with the
 *       distribution.
 *  
 *  THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 *  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 *  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 *  IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 *  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 *  IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "stddef.h"
#include "stdint.h"
#include "extlib.h"
#include "stdlib.h"
#include "console.h"
#include "serial.h"
#include "multiboot.h"
#include "gdt.h"
#include "interrupt.h"
#include "timer.h"
#include "mm/allocation.h"
#include "mm/paging.h"
#include "thread.h"
#include "loadelf.h"
#include "elf.h"
#include "syscall.h"
#include "exit.h"
#include "panic.h"
#include "trace.h"
#include "symtab.h"

static void read_mmap_info(struct mb_info *mbi)
{
    struct mb_mmap *region; // current memory region
    struct mb_mmap *end;    // end of memory region array
    region = (struct mb_mmap *) mbi->mmap_addr;
    end = (struct mb_mmap *) (mbi->mmap_addr + mbi->mmap_length);
    TRACE("Memory map from bootloader, at address %.8X:", mbi->mmap_addr);
    while (region < end){
        TRACE("    Region %.8X .. %.8X (type %d)",
          (unsigned long) region->base_addr,
          (unsigned long) (region->base_addr + region->length - 1),
          region->type);
        if (region->type == 1){
            pmem_init_mark_free(region->base_addr, region->base_addr + region->length);
        }
        // locate next region
        region = (struct mb_mmap *) (((char *) region) + region->size + 4);
    }
}

static void read_multiboot_info(struct mb_info *mbi)
{
    if (mbi->flags & MBI_BOOT_LOADER_NAME){
        // Bootloader has given us a name. Why not print it?
    }
    if (mbi->flags & MBI_MMAP_XXX){
        // read the memory map
        read_mmap_info(mbi);
    } else if (mbi->flags & MBI_MEM_XXX){
        // Use the more basic memory information
        console_printf(&tty1, "Upper memory (mbi->mem_upper): %d kiB\n", mbi->mem_upper);
        pmem_init_mark_free(KERNEL_PHYS_BASE, KERNEL_PHYS_BASE + mbi->mem_upper * 1024);
    } else {
        panic("The bootloader gave us no memory information!");
    }
}

static void read_multiboot_modules(struct mb_info *mbi)
{
    // XXX: we're assuming the first 4 MiB is identity-mapped!
    struct mb_module *mod;
    //static struct pagedir module_pd;
    int count, i;
    if (mbi->flags & MBI_MODS_XXX){
        // find address of first module structure from the multiboot info
        mod = (struct mb_module *) mbi->mods_addr;
        count = mbi->mods_count;
        for (i=0; i<count; ++i,++mod){
            // print info about each module
            TRACE("Module: %s at 0x%.8X (%d kiB)", (char *) mod->string, mod->mod_start,
              uldivru(mod->mod_end - mod->mod_start, 1024));
            // load only the first elf module
            if (i == 0){
                //pagedir_create(&module_pd);
                //load_elf_module(&module_pd, (void *) mod->mod_start, mod->mod_start, mod->mod_end - mod->mod_start);
                load_elf_kernel_module(
                  (void *) ((mod->mod_start + KERNEL_VIRT_BASE) - KERNEL_PHYS_BASE),
                   mod->mod_start,
                   mod->mod_end - mod->mod_start);
            }
        }
        if (count == 0){
            TRACE("No multiboot modules found.");
        }
    } else {
        TRACE("No multiboot module information.");
        TRACE("Either the bootloader doesn't support modules,");
        TRACE("or no modules were loaded.");
    }
}

static void read_kernel_symtab(struct mb_info *mbi)
{
    // XXX: assuming 1:1 mapping
    if (!(mbi->flags & MBI_ELF_SYMS)){
        panic("No ELF symbols provided by the bootloader");
    } else {
        struct Elf32_Shdr *shdr, *shstr, *symstr;
        struct Elf32_Sym *sym, *sym_end;
        unsigned int e_shnum = mbi->syms.elf.e_shnum, i;

        // get the section name string table section header (usually called .shstrtab)        
        shstr = (struct Elf32_Shdr *) (mbi->syms.elf.e_shaddr
          + mbi->syms.elf.e_shstrndx * mbi->syms.elf.e_shentsize);

        // Quick and rather featureless ELF symbol-table reader
        // This is not nice code, but it doesn't have to do much right now.
        for (i=1; i<e_shnum; ++i){
            // get the section header of this section
            shdr = (struct Elf32_Shdr *) (mbi->syms.elf.e_shaddr
              + i * mbi->syms.elf.e_shentsize);
            if (shdr->sh_type == SHT_SYMTAB){
                // we've found the symbol table

                // get the section header of the string table that contains the
                // symbol names
                symstr = (struct Elf32_Shdr *) (mbi->syms.elf.e_shaddr
                  + shdr->sh_link * mbi->syms.elf.e_shentsize);

                // get the first global symbol
                sym = (struct Elf32_Sym *) (shdr->sh_addr
                  + (shdr->sh_info * shdr->sh_entsize));

                // find the end of the symbol table
                sym_end = (struct Elf32_Sym *) (shdr->sh_addr + shdr->sh_size);

                // loop through all the symbols
                for (; sym < sym_end;
                  sym = (struct Elf32_Sym *) ((char *) sym + shdr->sh_entsize)){
                    symtab_insert((const char *) (symstr->sh_addr + sym->st_name),
                      sym->st_value);
                }
            }
        }
    }
}

static void pagefault_handler(int vector, struct interrupt_stack *is)
{
    console_colour_fg(&tty1, COLOUR_BRIGHT_RED);
    console_printf(&tty1, "%.8X caused PAGE FAULT at %.8X!\n",
      is->eip, get_cr2());
    panic("Unhandled page fault");
}

typedef int (* modinit_t)(void);
extern modinit_t _modinit_start[], _modinit_end[];

static void call_modinit(void)
{
    // call initialisation functions of statically-linked modules
    modinit_t *i;
    for (i=_modinit_start; i<_modinit_end; ++i){
        (*i)();
    }
}

void kmain(uint32_t freemem_base, struct mb_info *mbi, unsigned int magic)
{
    static struct pagedir init_pagedir;

    console_init(&tty1); // init the text console
    serial_init(&com1, 1, 9600); // try to init ttyS0 (first serial port)
    if (magic != MB_BOOT_MAGIC){
        console_colour_fg(&tty1, COLOUR_BRIGHT_RED);
        console_printf(&tty1, "Bootloader gave us an invalid magic number: 0x%.8X\n", magic);
        console_colour_default(&tty1);
    }
    pmem_init_set_freemem_base(freemem_base);
    read_multiboot_info(mbi);
    TRACE("Free memory: %d MiB", uldivru(pmem_get_size(), 1024 * 1024));
    mm_allocation_init();
    create_init_pagedir(&init_pagedir);

    // allocate a page for the IDT, GDT and TSS
    uintptr_t page_paddr, page_vaddr;
    void *page;
    size_t n_pages_phys, n_pages_virt;
    n_pages_phys = ppalloc(1, 1, &page_paddr);
    if (n_pages_phys == 0){
        panic("cannot allocate physical page for IDT/GDT/TSS");
    }
    n_pages_virt = vpalloc(&kvpa, 1, 1, &page_vaddr);
    if (n_pages_virt == 0){
        panic("cannot allocate virtual page for IDT/GDT/TSS");
    }
    map_mem(NULL, page_paddr, page_vaddr, 1, PTE_PRESENT | PTE_USER);
    page = (void *) page_vaddr;
    gdt_init(&page);
    interrupt_init(&page);
    syscall_init();

    // Map VRAM to a suitable place
    n_pages_virt = vpalloc(&kvpa, 1, 1, &page_vaddr);
    if (n_pages_virt == 0){
        panic("cannot allocate virtual page for VGA memory (0xB8000)");
    }
    map_mem(NULL, 0xB8000, page_vaddr, 1, PTE_PRESENT | PTE_WRITABLE);
    tty1.vram = (void *) page_vaddr; // TODO: don't probe into other modules' structures

    set_interrupt(0xE, pagefault_handler);

    timer_sys_start();
    thread_sys_init();
    symtab_init();
    read_kernel_symtab(mbi);
    read_multiboot_modules(mbi);

    call_modinit();

    thread_sys_start();
    asm volatile ("sti");

    // enter an idle loop, so we can see the effects of interrupts
    for (;;){
        asm volatile ("hlt");
    }
}
