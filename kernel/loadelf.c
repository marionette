/*
 *    Copyright (c) 2009 Joshua Phillips. All rights reserved.
 *  
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are
 *  met:
 *  
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in
 *       the documentation and/or other materials provided with the
 *       distribution.
 *  
 *  THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 *  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 *  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 *  IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 *  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 *  IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "loadelf.h"
#include "elf.h"
#include "archinf.h"
#include "mm/paging.h"
#include "thread.h"
#include "console.h"
#include "stdlib.h"
#include "extlib.h"
#include "string.h"
#include "symtab.h"
#include "trace.h"
#include "assert.h"
#include "panic.h"

extern char stack[];

struct elf_file {
    void *addr;     // base address of ELF file in memory
    struct Elf32_Ehdr *ehdr;
    struct Elf32_Shdr *common_shdr; // NOBITS section for putting COMMON symbols
    int common_size; // size of all the COMMON symbols
    int common_pos; // allocation position for COMMON symbols
};

static struct Elf32_Shdr *get_shdr(struct elf_file *restrict e, int n)
{
    if (n >= 1 && n < e->ehdr->e_shnum){
        return (struct Elf32_Shdr *) ((char *) e->addr + e->ehdr->e_shoff + n * e->ehdr->e_shentsize);
    } else {
        return NULL;
    }
}

static int get_shndx(struct elf_file *restrict e, struct Elf32_Shdr *shdr)
{
    int n = ((char *) shdr - ((char *) e->addr + e->ehdr->e_shoff)) / e->ehdr->e_shentsize;
    return n;
}

static void *get_section_data(struct elf_file *restrict e, struct Elf32_Shdr
  *shdr)
{
    if (shdr->sh_type == SHT_NOBITS){
        // section isn't actually allocated in the file
        return (char *) shdr->sh_addr;
    } else {
        return (char *) e->addr + shdr->sh_offset;
    }
}

static struct Elf32_Sym *symtab_get_sym(struct elf_file *restrict e, struct Elf32_Shdr
  *symtab, int n)
{
    int offset; // offset into section
    offset = n * symtab->sh_entsize;
    if (offset >= symtab->sh_size){
        return NULL;
    }
    return (struct Elf32_Sym *) ((char *) get_section_data(e, symtab) + offset);
}

static const char *get_string(struct elf_file *restrict e, struct Elf32_Shdr
  *strtab, int offset)
{
    if (offset <= 0 || offset >= strtab->sh_size){
        return NULL;
    } else {
        return (const char *) get_section_data(e, strtab) + offset;
    }
}

static const char *sym_get_name(struct elf_file *restrict e, struct Elf32_Shdr
  *symtab, struct Elf32_Sym *sym)
{
    struct Elf32_Shdr *strtab = get_shdr(e, symtab->sh_link);
    if (!strtab){
        TRACE("symbol table has no string table");
        return NULL;
    }
    return get_string(e, strtab, sym->st_name);
}

static int do_relocs(struct elf_file *restrict e, struct Elf32_Shdr *rel_shdr)
{
    struct Elf32_Shdr *symtab_shdr, *apply_shdr;
    struct Elf32_Rel *rel, *rel_end;
    struct Elf32_Sym *sym;
    const char *sym_name;
    uintptr_t value_from_symtab;
    Elf32_Addr value;

    symtab_shdr = get_shdr(e, rel_shdr->sh_link);
    apply_shdr = get_shdr(e, rel_shdr->sh_info);
    if (!symtab_shdr || !apply_shdr){
        TRACE("invalid sh_link/sh_info in relocation section");
        return 1;
    }

    // loop through all the relocations
    rel = get_section_data(e, rel_shdr);
    rel_end = (struct Elf32_Rel *) ((char *) rel + rel_shdr->sh_size);
    for (; rel < rel_end;
      rel = (struct Elf32_Rel *) ((char *) rel + (rel_shdr->sh_entsize))){

        // get the symbol that the relocation points to
        sym = symtab_get_sym(e, symtab_shdr, ELF32_R_SYM(rel->r_info));
        if (!sym){
            TRACE("invalid symbol in relocation (%d)", ELF32_R_SYM(rel->r_info));
            return 1;
        } else if (sym->st_shndx == SHN_UNDEF){
            // undefined symbol
            // let's try to find it in the kernel's symbol table
            sym_name = sym_get_name(e, symtab_shdr, sym);
            if (sym_name){
                value_from_symtab = symtab_lookup(sym_name);
                if (value_from_symtab){
                    value = value_from_symtab;
                    goto got_value; // http://xkcd.com/292/
                } else {
                    TRACE("undefined symbol: %s", sym_name);
                    return 1;
                }
            } else {
                TRACE("undefined, nameless symbol!");
                return 1;
            }
        } else {
            // find the raw symbol address
            if (sym->st_shndx == SHN_ABS){
                value = sym->st_value;
            } else {
                struct Elf32_Shdr *rel_shdr = get_shdr(e, sym->st_shndx);
                if (!rel_shdr){
                    TRACE("relocation points to invalid section (%d)", sym->st_shndx);
                    return 1;
                }
                value = rel_shdr->sh_addr + sym->st_value;
            }
got_value:
            // apply the relocation in the appropriate manner
            switch (ELF32_R_TYPE(rel->r_info)){
                case R_386_32:
                    *(uint32_t *) ((char *) get_section_data(e, apply_shdr)
                                            + rel->r_offset)
                                  += value;
                    break;
                case R_386_PC32:
                    *(int32_t *) ((char *) get_section_data(e, apply_shdr)
                                           + rel->r_offset)
                                  += value - (apply_shdr->sh_addr +
                                              rel->r_offset);
                    break;
                default:
                    TRACE("invalid relocation type: %d",
                      ELF32_R_TYPE(rel->r_info));
                    return 1;
            }
        }
    }
    return 0;
}

// Call a function for every symbol
static int foreach_sym(struct elf_file *restrict e, int (* func)(struct elf_file *restrict e, struct Elf32_Shdr *symtab, struct Elf32_Sym *sym, void *tag), void *tag)
{
    struct Elf32_Shdr *shdr, *symtab;
    struct Elf32_Sym *sym, *sym_end;
    int i, retval;

    shdr = get_shdr(e, 1);
    for (i=1; i<e->ehdr->e_shnum; ++i){
        if (shdr->sh_type == SHT_SYMTAB){
            symtab = shdr;

            sym = (struct Elf32_Sym *) get_section_data(e, symtab);
            sym_end = (struct Elf32_Sym *) ((char *) get_section_data(e, symtab)
              + symtab->sh_size);
            while (sym < sym_end){
                if ((retval = func(e, symtab, sym, tag))){
                    return retval;
                }
                sym = (struct Elf32_Sym *) ((char *) sym + symtab->sh_entsize);
            }
        }
        shdr = (struct Elf32_Shdr *) ((char *) shdr + e->ehdr->e_shentsize);
    }
    return 0;
}

// Find a global symbol by name

struct find_sym_struct {
    const char *name;
    struct Elf32_Sym *sym;
};

static int find_sym_func(struct elf_file *restrict e, struct Elf32_Shdr *symtab, struct Elf32_Sym *sym, void *tag)
{
    struct find_sym_struct *restrict s = tag;

    if (ELF32_ST_BIND(sym->st_info) == STB_GLOBAL){
        const char *sym_name;

        sym_name = sym_get_name(e, symtab, sym);
        if (sym_name && !strcmp(sym_name, s->name)){
            s->sym = sym;
            return 1;
        }
    }
    return 0;
}

static struct Elf32_Sym *find_sym(struct elf_file *restrict e, const char *name)
{
    struct find_sym_struct s;
    s.name = name;
    s.sym = NULL;
    foreach_sym(e, find_sym_func, &s);
    return s.sym;
}

static int count_common_size(struct elf_file *restrict e, struct Elf32_Shdr *symtab, struct Elf32_Sym *sym, void *tag)
{
    int *p_common_size = tag;
    if (sym->st_shndx == SHN_COMMON){
        *p_common_size += sym->st_size;
    }
    return 0;
}

static int set_section_addresses(struct elf_file *restrict e)
{
    struct Elf32_Shdr *shdr;
    int i;

    // set the section addresses
    shdr = get_shdr(e, 1);
    for (i=1; i<e->ehdr->e_shnum; ++i){
        if (shdr->sh_type == SHT_PROGBITS){
            shdr->sh_addr = (Elf32_Addr) e->addr + shdr->sh_offset;
        } else if (shdr->sh_type == SHT_NOBITS){
            // we have to allocate our own memory for this section
            if (e->common_shdr == NULL){
                // we have to include the common symbols, too
                e->common_shdr = shdr;
                e->common_pos = shdr->sh_size;
                shdr->sh_size += e->common_size;
            }
            void *nobits_data = malloc(shdr->sh_size);
            if (!nobits_data){
                TRACE("could not allocate data (%d bytes) for SHT_NOBITS section", shdr->sh_size);
                return 1;
            }
            shdr->sh_addr = (Elf32_Addr) nobits_data;
        }
        shdr = (struct Elf32_Shdr *) ((char *) shdr + e->ehdr->e_shentsize);
    }
    return 0;
}

static int allocate_common_symbols(struct elf_file *restrict e, struct Elf32_Shdr *symtab, struct Elf32_Sym *sym, void *tag)
{
    if (sym->st_shndx == SHN_COMMON){
        assert(e->common_shdr);
        sym->st_shndx = get_shndx(e, e->common_shdr);
        sym->st_value = e->common_pos;
        e->common_pos += sym->st_size;
    }
    return 0;
}

int load_elf_kernel_module(void *vaddr, uintptr_t paddr, size_t size)
{
    struct elf_file _e, *e = &_e;
    struct Elf32_Ehdr *ehdr = vaddr;
    struct Elf32_Sym *entry_sym;
    struct Elf32_Shdr *shdr;
    int i;
    uintptr_t entry_addr;

    e->addr = vaddr;
    e->ehdr = ehdr;
    
    // verify magic bytes
    if (ehdr->e_ident[EI_MAG0] == ELFMAG0
      && ehdr->e_ident[EI_MAG1] == ELFMAG1
      && ehdr->e_ident[EI_MAG2] == ELFMAG2
      && ehdr->e_ident[EI_MAG3] == ELFMAG3){
        // ok
    } else {
        TRACE("Not an ELF file.");
        return 1;
    }
    if (ehdr->e_ident[EI_CLASS] != ELFCLASS32){
        TRACE("invalid or unsupported ELF class");
        return 1;
    }
    if (ehdr->e_ident[EI_DATA] != ELFDATA2LSB){
        TRACE("invalid or unsupported ELF data encoding");
        return 1;
    }
    if (ehdr->e_type != ET_REL){
        TRACE("ELF file is not a relocatable. It's not a valid kernel module.");
        return 1;
    }
    if (ehdr->e_machine != EM_386){
        TRACE("ELF file is not for a 386. I can't execute it.");
        return 1;
    }

    // preliminary pass to find out how much memory we need for COMMON symbols
    e->common_size = 0;
    foreach_sym(e, count_common_size, &e->common_size);
    e->common_shdr = NULL;

    // set the section addresses and allocate NOBITS sections
    int ret = set_section_addresses(e);
    if (ret){
        return ret;
    }

    // allocate COMMON symbols
    foreach_sym(e, allocate_common_symbols, NULL);
    assert(e->common_size == e->common_pos);

    // TODO: we really need to do some more clearing up on failure
    // and, of course, when the module is unloaded.

    // Do relocations
    shdr = get_shdr(e, 1);
    for (i=1; i<ehdr->e_shnum; ++i){
        if (shdr->sh_type == SHT_REL){
            if (do_relocs(e, shdr) != 0){
                return 1;
            }
        } else if (shdr->sh_type == SHT_RELA){
            panic("SHT_RELA not supported!");
        }
        shdr = (struct Elf32_Shdr *) ((char *) shdr + ehdr->e_shentsize);
    }

    // Run _start
    entry_sym = find_sym(e, "_start");
    if (!entry_sym){
        TRACE("kernel module has no symbol _start");
        return 1;
    } else {
        if (entry_sym->st_shndx == SHN_UNDEF){
            TRACE("_start is undefined!");
            return 1;
        } else if (entry_sym->st_shndx == SHN_ABS){
            entry_addr = entry_sym->st_value;
        } else {
            shdr = get_shdr(e, entry_sym->st_shndx);
            if (!shdr){
                TRACE("_start is in an invalid section");
                return 1;
            }
            entry_addr = (uintptr_t) get_section_data(e, shdr) + entry_sym->st_value;
        }
        typedef void (* voidfunc_t)(void);
        ((voidfunc_t) entry_addr)();
    }

    return 0;
}

int load_elf_module(struct pagedir *target_pd, void *vaddr, uintptr_t paddr, size_t size)
{
    struct Elf32_Ehdr *ehdr = vaddr;
    struct Elf32_Phdr *phdr;
    int i;
    struct thread *module_thread;

    // verify magic bytes
    if (ehdr->e_ident[EI_MAG0] == ELFMAG0
      && ehdr->e_ident[EI_MAG1] == ELFMAG1
      && ehdr->e_ident[EI_MAG2] == ELFMAG2
      && ehdr->e_ident[EI_MAG3] == ELFMAG3){
        // ok
    } else {
        TRACE("Not an ELF file.");
        return 1;
    }
    if (ehdr->e_ident[EI_CLASS] != ELFCLASS32){
        TRACE("invalid or unsupported ELF class");
        return 1;
    }
    if (ehdr->e_ident[EI_DATA] != ELFDATA2LSB){
        TRACE("invalid or unsupported ELF data encoding");
        return 1;
    }
    if (ehdr->e_type != ET_EXEC){
        TRACE("ELF file is not an executable. I can't, erm, execute it.");
        return 1;
    }
    if (ehdr->e_machine != EM_386){
        TRACE("ELF file is not for a 386. I can't execute it.");
        return 1;
    }
    // map program segments into the correct places
    phdr = (struct Elf32_Phdr *) ((char *) vaddr + ehdr->e_phoff);
    for (i=0; i<ehdr->e_phnum; ++i){
        if (phdr->p_type == PT_DYNAMIC){
            TRACE("ELF file requires dynamic linking, and I haven't written a dynamic linker yet.");
            return 1;
        } else if (phdr->p_type == PT_PHDR){
            TRACE("ELF file has a PT_PHDR segment; not implemented yet.");
            return 1;
        } else if (phdr->p_type == PT_LOAD){
            // TODO: don't hard-code, please
            if (phdr->p_vaddr > 0xC0000000){
                TRACE("ELF file wants to load above 0xC0000000");
                return 1;
            } else if (phdr->p_vaddr < 0x1000){
                TRACE("ELF file wants to load in NULL page.");
                return 1;
            } else {
                // TODO: load bss segments: phdr->p_filesz == 0
                unsigned int map_flags = PTE_PRESENT | PTE_USER;
                if (phdr->p_flags & PF_W){
                    map_flags |= PTE_WRITABLE;
                }
                if (map_mem(target_pd,
                  paddr + phdr->p_offset, // physical
                  phdr->p_vaddr,          // virtual
                  uldivru(phdr->p_memsz, PAGE_SIZE), // n_pages
                  map_flags) != 0){
                    TRACE("map_mem() failed - cannot map ELF segment");
                }
            }
        } else {
            TRACE("Unknown segment type: %d!", phdr->p_type);
        }
        phdr = (struct Elf32_Phdr *) ((char *) phdr + ehdr->e_phentsize);
    }

    // create a task to run the module in
    module_thread = thread_new();
    thread_set_pagedir(module_thread, target_pd);
    thread_set_eip(module_thread, ehdr->e_entry);
    // XXX: Guessing stack size, blah blah == bad
    thread_set_esp(module_thread, (uint32_t) stack + 0x4000);
    thread_set_userspace(module_thread, 1);
    thread_resume(module_thread);
    return 0;
}
