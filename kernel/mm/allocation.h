/*
 *    Copyright (c) 2008 Joshua Phillips. All rights reserved.
 *  
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are
 *  met:
 *  
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in
 *       the documentation and/or other materials provided with the
 *       distribution.
 *  
 *  THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 *  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 *  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 *  IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 *  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 *  IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 *  Takes care of the major allocation work:
 *    - physical page allocation
 *    - virtual address space allocation
 *  NOTE: malloc does not belong here.
 *
 *  Although struct vpallocator uses struct buddy, this is to avoid using
 *  pointers, and the buddy allocator should be kept as separate as possible.
 */

#ifndef MM_ALLOCATION_H
#define MM_ALLOCATION_H

#include "stddef.h"
#include "stdint.h"

extern int _phys_base[]; // defined in linker script
extern int _virt_base[]; // defined in linker script

#define KERNEL_PHYS_BASE ((uintptr_t) _phys_base)
#define KERNEL_VIRT_BASE ((uintptr_t) _virt_base)

// these are called during startup from the code which reads
// memory information (eg. from multiboot information)

void pmem_init_set_freemem_base(uintptr_t freemem_base);
void pmem_init_mark_free(uintptr_t region_start, uintptr_t region_end);
size_t pmem_get_size(void); // return size of physical memory
void mm_allocation_init(void); // main initialization function

///// Physical Page Allocation /////

// Allocate between min_pages and max_pages continuous physical pages, returns
// the number of pages allocated. If you want to allocate an exact
// number of pages, use the same number for min_pages and max_pages.
size_t ppalloc(size_t min_pages, size_t max_pages, uintptr_t *start_addr);

// Free pages allocated with alloc_pages
void ppfree(uintptr_t start_addr, size_t n_pages);

///// Virtual Page Allocation /////

// A virtual page allocator.
// Use one for every address space:
//  - one kernel allocator
//  - one for each user address space
struct vpallocator {
    uintptr_t base;
    struct buddy *buddy;
};

// Kernel virtual page allocator
extern struct vpallocator kvpa;

// create a vpallocator to manage addresses starting at 'base' and
// covering 'extent' pages.
// Return value: 0 = success
int vpcreate(struct vpallocator *restrict vp, uintptr_t base, size_t extent);

// Allocate/free virtual pages (NOTE: there is no physical memory backing these pages!)
size_t vpalloc(struct vpallocator *restrict vp, size_t min_pages, size_t max_pages, uintptr_t *start_addr);
void vpfree(struct vpallocator *restrict vp, uintptr_t start_addr, size_t n_pages);

#endif
