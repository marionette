/*
 *    Copyright (c) 2008 Joshua Phillips. All rights reserved.
 *  
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are
 *  met:
 *  
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in
 *       the documentation and/or other materials provided with the
 *       distribution.
 *  
 *  THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 *  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 *  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 *  IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 *  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 *  IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "mm/buddy.h"
#include "extlib.h"
#include "stdlib.h"
#include "string.h"

static inline int bit_test(bit_t x, int n)
{
    return (x >> n) & 1;
}

static inline void bit_set(bit_t *x, int n, unsigned int value)
{
    *x = (*x & ~(1 << n)) | (value << n);
}

// for a buddy system of n_chucks leaf nodes,
// return *height - the height of the tree
// return *struct_size - the size, in bytes, required by the
// allocator to store all the metadata.
// n_chucks must be a power of two (will fix later)
void get_buddy_size(size_t n_chunks, int *height, size_t *struct_size)
{
    size_t n_chunks_rounded = 1 << ilog2up(n_chunks);
    *height = ilog2(n_chunks_rounded);
    *struct_size = sizeof(struct buddy);
    if (n_chunks_rounded <= BITS){
        // each bitmap will be only one int
        *struct_size += *height * sizeof(bit_t);
    } else {
        *struct_size += (LOG_BITS + ((n_chunks_rounded / (BITS / 2)) - 2)) * sizeof(bit_t);
    }
}

// get offset into bitmaps[] for start of row_n
static inline int buddy_get_row_offs(struct buddy *buddy, int row_n)
{
    if (row_n <= LOG_BITS){
        // each bitmap will be only one int
        return row_n;
    } else {
        return LOG_BITS + ((1 << (row_n - (LOG_BITS - 1))) - 2);
    }
}

static inline int buddy_get_bit(struct buddy *buddy, int row_offs, int n)
{
    return bit_test((buddy->bitmaps + row_offs)[n / BITS], n % BITS);
}

static inline void buddy_set_bit(struct buddy *buddy, int row_offs, int n, unsigned int value)
{
    bit_set(&(buddy->bitmaps + row_offs)[n / BITS], n % BITS, value);
}

void buddy_init(struct buddy *buddy, size_t n_chunks, int height, size_t struct_size)
{
    size_t n_chunks_rounded = 1 << ilog2up(n_chunks);
    buddy->bitmaps = (bit_t *)(buddy + 1);
    buddy->n_chunks = n_chunks_rounded;
    buddy->height = height;
    memset(buddy->bitmaps, 0, struct_size - sizeof(struct buddy));
    if (n_chunks_rounded > n_chunks){
        buddy_mark_used(buddy, n_chunks, n_chunks_rounded - n_chunks);
    }
}

// Will try allocating max_size units (rounded up to nearest power of two)
// If it fails, it'll try allocating smaller amounts no smaller than min_size.
// It will return the allocated size in *allocated_size. The caller *must*
// store this value, because it is needed to pass to buddy_free.
// It will return -1 on failure.
// The purpose of allocating a smaller chunk is for virtual memory, where it
// is desirable to allocate physical pages contiguoulsy (to aid caching), but
// not essential; if buddy_alloc allocates less than the desired size, the
// caller can call buddy_alloc again, for the remainder of the data.
unsigned int buddy_alloc(struct buddy *buddy, size_t min_size, size_t max_size, size_t *allocated_size)
{
    int row_n;
    unsigned int i, k;
    int j;
    unsigned int row_len, row_offs;
    size_t size;
    size = 1 << ilog2up(max_size);
    min_size = 1 << ilog2(min_size);
    if (size == 0 || size > (buddy->n_chunks / 2)){
        return -1;
    }
top:
    // locate row of desired size
    row_n = buddy->height - ilog2up(size) - 1;
    row_len = uldivru(buddy->n_chunks, size);
    row_offs = buddy_get_row_offs(buddy, row_n);
    for (i=0; i<row_len; i++){
        if (!buddy_get_bit(buddy, row_offs, i)){
            unsigned int shift;
            // Set this node, and children, as used
            for (j=row_n,shift=0; j<buddy->height; j++,shift++){
                unsigned int row_offs_2 = buddy_get_row_offs(buddy, j);
                for (k=(i<<shift); k<((i+1)<<shift); k++){
                    buddy_set_bit(buddy, row_offs_2, k, 1);
                }
            }
            // Set parents as used
            for (j=row_n-1,shift=1; j>=0; j--,shift++){
                unsigned int row_offs_2 = buddy_get_row_offs(buddy, j);
                buddy_set_bit(buddy, row_offs_2, i >> shift, 1);
            }
            *allocated_size = size;
            return i * size;
        }
    }
    // Oh dear. Let's try smaller size (XXX: is there a more efficient way,
    // instead of starting again?)
    size /= 2;
    if (size > min_size)
        goto top;
    return -1;
}

void buddy_free(struct buddy *buddy, unsigned int o, size_t size)
{
    unsigned int shift, i, row_n;
    int j;
    size = 1 << ilog2up(size);
    row_n = buddy->height - ilog2up(size) - 1;
    i = o / size; // XXX: make sure size really is a power of two
    // set this node, and children, as free
    for (j=row_n,shift=0; j<buddy->height; j++,shift++){
        unsigned int k;
        unsigned int row_offs_2 = buddy_get_row_offs(buddy, j);
        for (k=(i<<shift); k<((i+1)<<shift);k++){
            buddy_set_bit(buddy, row_offs_2, k, 0);
        }
    }
    // update parent nodes
    for (j=row_n-1,shift=1; j>=0; j--,shift++){
        unsigned int row_offs_2 = buddy_get_row_offs(buddy, j);
        unsigned int row_offs_1 = buddy_get_row_offs(buddy, j+1);
        // Is our buddy bit set?
        if (buddy_get_bit(buddy, row_offs_1, (i >> (shift - 1)) ^ 1)){
            // yes, so we'll stop now
            break;
        } else {
            // no! Clear parent bit! (coalesce)
            buddy_set_bit(buddy, row_offs_2, i >> shift, 0);
        }
    }
}

void buddy_mark_used(struct buddy *buddy, unsigned int start, size_t count)
{
    int row_n = buddy->height - 1;
    unsigned int row_offs = buddy_get_row_offs(buddy, row_n);
    unsigned int i;
    int j;
    unsigned int shift;
    for (i=start; i<(start+count); i++){
        buddy_set_bit(buddy, row_offs, i, 1);
        // set parents as used
        for (j=row_n-1,shift=1; j>=0; j--,shift++){
            unsigned int row_offs_2 = buddy_get_row_offs(buddy, j);
            buddy_set_bit(buddy, row_offs_2, i >> shift, 1);
        }
    }
}

