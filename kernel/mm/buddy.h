/*
 *    Copyright (c) 2008 Joshua Phillips. All rights reserved.
 *  
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are
 *  met:
 *  
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in
 *       the documentation and/or other materials provided with the
 *       distribution.
 *  
 *  THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 *  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 *  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 *  IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 *  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 *  IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef MM_BUDDY_H
#define MM_BUDDY_H

#include "stddef.h"

// architecture dependant!
typedef unsigned long bit_t;
#define BITS 32
#define LOG_BITS 5

struct buddy {
    size_t n_chunks; // number of leaf nodes
    int height; // tree height (number of bitmaps[] entries)
    bit_t *bitmaps;
};

void get_buddy_size(size_t n_chunks, int *height, size_t *struct_size);
void buddy_init(struct buddy *buddy, size_t n_chunks, int height, size_t struct_size);
unsigned int buddy_alloc(struct buddy *buddy, size_t min_size, size_t max_size, size_t *allocated_size);
void buddy_free(struct buddy *buddy, unsigned int o, size_t size);
void buddy_mark_used(struct buddy *buddy, unsigned int start, size_t count);

#endif

