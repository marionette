/*
 *    Copyright (c) 2009 Joshua Phillips. All rights reserved.
 *  
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are
 *  met:
 *  
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in
 *       the documentation and/or other materials provided with the
 *       distribution.
 *  
 *  THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 *  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 *  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 *  IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 *  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 *  IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 *  Allocates kernel virtual memory, and physical memory, in units of pages,
 *  and performs a mapping. (Like malloc, but for pages)
 */

#include "mm/kvmalloc.h"
#include "mm/allocation.h"
#include "mm/paging.h"
#include "archinf.h"

size_t kvmalloc(size_t min_pages, size_t max_pages,
  void **ptr_out, uintptr_t *phys_array)
{
    // TODO: call alloc_pages repeatedly to try to get up to max_pages
    uintptr_t phys_addr, virt_addr;
    size_t n_phys, n_virt;
    unsigned int i;

    // allocate physical pages
    n_phys = ppalloc(min_pages, max_pages, &phys_addr);
    if (n_phys == 0){
        // failed
        return 0;
    }
    // allocate virtual pages to match
    n_virt = vpalloc(&kvpa, min_pages, n_phys, &virt_addr);
    if (n_virt == 0){
        // failed
        ppfree(phys_addr, n_phys);
        return 0;
    } else if (n_virt < n_phys){
        // free some of our physical pages
        // XXX: this assumes ppfree won't mind freeing half an
        // allocated region. For my buddy allocator, this is true.
        ppfree(phys_addr + n_virt * PAGE_SIZE, n_phys - n_virt);
        n_phys = n_virt;
    }
    // store the physical addresses
    if (phys_array){
        for (i=0; i<n_phys; i++){
            phys_array[i] = phys_addr + i * PAGE_SIZE;
        }
    }
    // perform the mapping
    if (map_mem(NULL, phys_addr, virt_addr, n_phys, PTE_PRESENT | PTE_WRITABLE) != 0){
        // oh dear - couldn't even map :(
        ppfree(phys_addr, n_phys);
        vpfree(&kvpa, virt_addr, n_virt);
        return 0;
    }
    *ptr_out = (void *) virt_addr;
    return n_phys;
}

void kvfree(void *ptr, size_t n_allocated)
{
    // free a physical page at a time, getting the address
    // out the page table
    // XXX: this assumes free_pages won't mind freeing half an
    // allocated region. For my buddy allocator, this is true.
    uintptr_t virt_addr = (uintptr_t) ptr;
    uintptr_t phys_addr;
    size_t i;

    for (i=0; i<n_allocated; i++){
        // fetch physical address and free it
        phys_addr = xlat_v2p(NULL, virt_addr);
        ppfree(phys_addr, 1);
        // next page
        virt_addr += PAGE_SIZE;
    }
    // now we can perform an unmap
    map_mem(NULL, 0, (uintptr_t) ptr, n_allocated, 0);
    // and finally, free the virtual pages
    vpfree(&kvpa, (uintptr_t) ptr, n_allocated);
}
