#include "stdlib.h"
#include "trace.h"
#include "mm/kvmalloc.h"
#include "archinf.h"
#include "assert.h"
#include "string.h"

// dlmalloc settings
#define LACKS_UNISTD_H
#define LACKS_FCNTL_H
#define LACKS_SYS_PARAM_H
#define LACKS_SYS_MMAN_H
#define LACKS_STRINGS_H
#define LACKS_STRING_H
#define LACKS_SYS_TYPES_H
#define LACKS_ERRNO_H
#define LACKS_STDLIB_H
#define LACKS_STDIO_H

#define HAVE_MMAP 1
#define HAVE_MREMAP 0
#define HAVE_MORECORE 0
#define USE_LOCKS 0
#define NO_MALLINFO 1
#define ABORT_ON_ASSERT_FAILURE 1

#define ABORT panic("Doug Lee's memory allocator aborted!")
#define malloc_getpagesize ((size_t) PAGE_SIZE)
#define MALLOC_FAILURE_ACTION

#define fprintf(f, fmt, ...) 
#define malloc dlmalloc
#define realloc dlrealloc
#define free dlfree

// turn dlmalloc debugging off
#undef DEBUG
#define DEBUG 0

// a fake mmap
#define mmap fake_mmap
#define munmap fake_munmap
#define PROT_EXEC 0x4
#define PROT_READ 0x1
#define PROT_WRITE 0x2
#define PROT_NONE 0x0
#define MAP_ANONYMOUS 0x20
#define MAP_PRIVATE 0x02


static void *fake_mmap(void *start, size_t length, int prot, int flags,
  int fd, int offset)
{
	void *ptr;
	size_t n;
	assert(start == NULL);
	assert(flags == (MAP_ANONYMOUS | MAP_PRIVATE));
	assert((length % PAGE_SIZE) == 0);
	n = kvmalloc(length / PAGE_SIZE, length / PAGE_SIZE, &ptr, NULL);
	if (n != (length / PAGE_SIZE)){
		TRACE("kvmalloc failed");
		if (n != 0){
			kvfree(ptr, n);
		}
		return NULL;
	} else {
		return ptr;
	}
}

static int fake_munmap(void *start, size_t length)
{
	// XXX: if dlmalloc doesn't call munmap
	// with the same parameters as mmap, then we have
	// to assume the buddy allocator doesn't mind.
	// Currently, this is so, but it may not be, in the future.
	kvfree(start, length);
	return 0;
}

#undef assert // dlmalloc has his own
#include "dlmalloc.inc"

// TODO: use locks to avoid re-entrance

void *malloc(size_t size)
{
    return dlmalloc(size);
}

void *realloc(void *ptr, size_t new_size)
{
    return dlrealloc(ptr, new_size);
}

void free(void *ptr)
{
    dlfree(ptr);
}
