/*
 *    Copyright (c) 2008 Joshua Phillips. All rights reserved.
 *  
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are
 *  met:
 *  
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in
 *       the documentation and/or other materials provided with the
 *       distribution.
 *  
 *  THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 *  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 *  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 *  IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 *  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 *  IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "mm/paging.h"
#include "mm/allocation.h"
#include "mm/kvmalloc.h"
#include "archinf.h"
#include "trace.h"
#include "assert.h"
#include "stdbool.h"
#include "stdlib.h" // for malloc!
#include "string.h"

// Keep a list of all page directories, so that we can update the kernel part
// of all of them at once.
static struct pagedir *pagedir_first = NULL, *pagedir_last = NULL;

static struct vpde kvpd[PDE_N_KERNEL];

// This points to any page directory. Since the kernel portions of each
// page directory are kept synchronized, this can point to any valid
// page directory.
static pde_t *kernel_pagedir;

void create_init_pagedir(struct pagedir *ipd)
{
    pagedir_first = pagedir_last = ipd;
    ipd->prev = ipd->next = NULL;

    ipd->phys_addr = get_cr3();

    // the first page directory was created during startup,
    // when it was easy to work out the virtual address of something
    ipd->virt_addr = (pde_t *) ((ipd->phys_addr
                                 + KERNEL_VIRT_BASE) - KERNEL_PHYS_BASE);
    //TRACE("ipd->virt_addr = %.8X", ipd->virt_addr);

    memset(ipd->vpd, 0, sizeof ipd->vpd);

    // we can also re-construct the mapping that was created in start.S
    ipd->vpd[0].vaddr = (((((pde_t *) ipd->virt_addr)[0] & PDE_ADDRESS)
                          + KERNEL_VIRT_BASE) - KERNEL_PHYS_BASE
                        ) / PAGE_SIZE;
    //TRACE("1:1 page table is at %.8X virtual", ipd->vpd[0].vaddr * PAGE_SIZE);
    
    memset(kvpd, 0, sizeof kvpd);
    kvpd[0].vaddr = (((((pde_t *) ipd->virt_addr)[PDE_KERNEL_FIRST] & PDE_ADDRESS)
                      + KERNEL_VIRT_BASE) - KERNEL_PHYS_BASE
                    ) / PAGE_SIZE;
    //TRACE("kernel page table is at %.8X virtual", kvpd[0].vaddr * PAGE_SIZE);

    kernel_pagedir = ipd->virt_addr;
}

int pagedir_create(struct pagedir *page_directory)
{
    size_t n_pages;

    // allocate the actual page directory (a whole page)
    // XXX: these casts are horrible. There must be a cleaner way!
    n_pages = kvmalloc(1, 1, (void **) (void *) &page_directory->virt_addr,
      &page_directory->phys_addr);
    if (n_pages == 0){
        TRACE("kvmalloc failed");
        return 1;
    }
    // clear user part of page directory
    memset(page_directory->virt_addr, 0, PDE_N_USER * sizeof(pde_t));
    memset(page_directory->vpd, 0, sizeof page_directory->vpd);
    
    // construct the kernel part of the page directory from the current
    // page directory. Since the kernel part is kept consistent, it doesn't
    // really matter where we get this from.
    memcpy(page_directory->virt_addr + PDE_KERNEL_FIRST,
      kernel_pagedir + PDE_KERNEL_FIRST, PDE_N_KERNEL * sizeof(pde_t));
    
    // add this page directory to our list, so the kernel part of it gets update
    // in the future.
    page_directory->prev = NULL;
    page_directory->next = pagedir_first;
    if (pagedir_first){
        pagedir_first->prev = page_directory;
    } else {
        pagedir_last = page_directory;
    }
    pagedir_first = page_directory;

    return 0;
}

void pagedir_destroy(struct pagedir *pagedir)
{
    // remove it from the list
    if (pagedir->prev){
        pagedir->prev->next = pagedir->next;
    } else {
        pagedir_first = pagedir->next;
    }
    if (pagedir->next){
        pagedir->next->prev = pagedir->prev;
    } else {
        pagedir_last = pagedir->prev;
    }
    // free the page directory (TODO: make sure it's not
    // currently in use!)
    kvfree(pagedir->virt_addr, 1);
}

// return true if there's a page table n
static inline bool pt_exists(const struct pagedir *restrict pd, int n)
{
    if (n >= 768){
        ///// kernel region /////
        return kvpd[n - PDE_KERNEL_FIRST].vaddr != 0;
    } else {
        ///// user region /////
        return pd->vpd[n].vaddr != 0;
    }
}

// set a PDE
static inline void set_pt(struct pagedir *pd, int pt_n, uintptr_t pt_phys, uintptr_t pt_virt)
{
    pde_t pde = (pt_phys & PDE_ADDRESS)
      | PDE_PRESENT | PDE_WRITABLE | PDE_USER;
    if (pt_n < PDE_N_USER){
        // user
        assert(pd != NULL);
        pd->virt_addr[pt_n] = pde;
        pd->vpd[pt_n].vaddr = pt_virt / PAGE_SIZE;
    } else {
        // kernel
        // update all page directories!
        struct pagedir *pd2;
        for (pd2 = pagedir_first;
          pd2;
          pd2 = pd2->next){
            pd2->virt_addr[pt_n] = pde;
        }
        kvpd[pt_n - PDE_KERNEL_FIRST].vaddr = pt_virt / PAGE_SIZE;
    }
}

static inline struct vpde *get_vpde(struct pagedir *pd, uintptr_t virtual)
{
    if (virtual < KERNEL_VIRT_BASE){
        // user
        assert(pd != NULL);
        return pd->vpd + (virtual / (PAGE_SIZE * PDE_N));
    } else {
        // kernel
        return kvpd + ((virtual / (PAGE_SIZE * PDE_N))
                       - PDE_KERNEL_FIRST);
    }
}

static inline pte_t *get_pte(struct pagedir *pd, uintptr_t virtual)
{
    pte_t *pt = (pte_t *)
      (get_vpde(pd, virtual)->vaddr * PAGE_SIZE);

    if (!pt){
        TRACE("get_vpde returned NULL for PDE[%d]", (virtual / (PAGE_SIZE * PDE_N)));
        return NULL; // even the page table is not present
    }
    return pt + ((virtual % (PAGE_SIZE * PDE_N)) / PAGE_SIZE);
}

int map_mem(struct pagedir *pd, uintptr_t physical, uintptr_t virtual, size_t n_pages, int flags)
{
    size_t n_new_pagetab = 0; // number of new page tables to create
    int pt_n, prev_pt_n;
    bool must_flush;
    pte_t *p_pte;

    size_t pp_allocated, vp_allocated;
    uintptr_t pp_start_addr, vp_start_addr;

    ///// Pass 1 /////

    // find out how many page tables that we need, that don't
    // already exist
    pt_n = virtual / (PAGE_SIZE * PDE_N);
    while ((pt_n * PDE_N) < ((virtual / PAGE_SIZE) + n_pages)){
        if (!pt_exists(pd, pt_n)){
            ++n_new_pagetab;
        } else {
        }
        ++pt_n;
    }

    if (n_new_pagetab > 0){
        TRACE("need %d new page tables", n_new_pagetab);

        // allocate the page tables in one go
        // TODO: use min_pages < max_pages
        pp_allocated = ppalloc(n_new_pagetab,
                               n_new_pagetab,
                               &pp_start_addr);
        if (pp_allocated < n_new_pagetab){
            TRACE("ppalloc failed");
            return 1;
        }

        vp_allocated = vpalloc(&kvpa, n_new_pagetab,
          n_new_pagetab, &vp_start_addr);
        if (vp_allocated < n_new_pagetab){
            TRACE("vpalloc failed");
            ppfree(pp_start_addr, pp_allocated);
            return 1;
        }

        // let's hope we don't hit infinite recursion
        if (map_mem(NULL, pp_start_addr, vp_start_addr,
              n_new_pagetab, PTE_PRESENT | PTE_WRITABLE) != 0){
            TRACE("map_mem for page tables failed");
            vpfree(&kvpa, vp_start_addr, vp_allocated);
            ppfree(pp_start_addr, pp_allocated);
            return 1;
        }
    }

    ///// Pass 2 /////

    prev_pt_n = -1;
    while (n_pages){
        // get page table number for this address
        pt_n = virtual / (PAGE_SIZE * PDE_N);
        // is this address managed by a different page table?
        if (pt_n != prev_pt_n){
            // we might need a new page table
            if (!pt_exists(pd, pt_n)){
                set_pt(pd, pt_n, pp_start_addr, vp_start_addr);
                pp_start_addr += PAGE_SIZE;
                vp_start_addr += PAGE_SIZE;
                prev_pt_n = pt_n;
            }
        }

        p_pte = get_pte(pd, virtual);
        assert(p_pte != NULL);
        
        // is the page already present?
        if (*p_pte & PTE_PRESENT){
            // the page is already present, so we need
            // to flush the TLB. Currently only
            // global flushes are used.
            must_flush = true;
        }

        // set the page table entry
        *p_pte = (physical & PTE_ADDRESS) | flags;

        // move on to the next page
        virtual += PAGE_SIZE;
        physical += PAGE_SIZE;
        --n_pages;
    }

    if (must_flush){
        // we've decided we need to flush the TLB
        set_cr3(get_cr3());
    }

    // Thank you very much. Enjoy the mapping.
    return 0;
}

uintptr_t xlat_v2p(struct pagedir *pd, uintptr_t virtual)
{
    pte_t *pte = get_pte(pd, virtual);
    if (!pte){
        return 0;
    }
    return *pte & PTE_ADDRESS;
}

void pagedir_switch(struct pagedir *new_pagedir)
{
    set_cr3(new_pagedir->phys_addr);
}
