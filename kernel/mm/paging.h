/*
 *    Copyright (c) 2008 Joshua Phillips. All rights reserved.
 *  
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are
 *  met:
 *  
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in
 *       the documentation and/or other materials provided with the
 *       distribution.
 *  
 *  THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 *  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 *  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 *  IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 *  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 *  IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 *  This deals with paging (mapping virtual addresses to physical memory)
 */

#ifndef MM_PAGING
#define MM_PAGING

#include "stddef.h"
#include "stdint.h"

// Types for page directory entries and
// page table entries (they're almost the same format,
// but it's clearer if we use two different identifiers)
typedef uint32_t pte_t, pde_t;

// page table/directory format
// as defined in the documentation for the CPU
#define PTE_PRESENT (1 << 0)        // page is valid
#define PTE_WRITABLE (1 << 1)       // write access bit
#define PTE_USER (1 << 2)           // user (ring1 - ring3) accessible
#define PTE_WRITE_THROUGH (1 << 3)
#define PTE_CACHE_DISABLED (1 << 4)
#define PTE_ACCESSED (1 << 5)       // set by CPU when page is accessed
#define PTE_DIRTY (1 << 6)          // set by CPU when page is written to
#define PTE_PAT (1 << 7)
#define PTE_GLOBAL (1 << 8)
#define PTE_ADDRESS 0xFFFFF000

#define PDE_PRESENT (1 << 0)
#define PDE_WRITABLE (1 << 1)
#define PDE_USER (1 << 2)
#define PDE_WRITE_THROUGH (1 << 3)
#define PDE_CACHE_DISABLED (1 << 4)
#define PDE_ACCESSED (1 << 5)
#define PDE_PAGESIZE (1 << 7)
#define PDE_GLOBAL (1 << 8)
#define PDE_ADDRESS 0xFFFFF000

// XXX: these values are set here, but if the
// kernel base address changes, these will
// change too!
#define PDE_N 1024        // number of PDEs
#define PDE_N_KERNEL 256  // number of kernel PDEs
#define PDE_N_USER 768    // number of user PDEs
#define PDE_KERNEL_FIRST 768

// Store a data structure mirroring the page directory
// and page tables, but with virtual addresses.
struct vpde {
    unsigned unused: 12; // this may be useful
    unsigned vaddr : 20; // TODO: vaddr is in kernel address space - save 2 bits or more!
};

struct pagedir {
    struct pagedir *prev, *next; // linked list (so we can update all page directories at once)
    uintptr_t phys_addr; // physical address of page directory
    pde_t *virt_addr; // kernel-virtual address of page directory
    struct vpde vpd[768];
};

// functions to get/set cr2 and cr3
// cr2 contains the virtual address of the last page fault.
// cr3 contains the address of the page directory.

static inline uint32_t get_cr2(void)
{
    uint32_t old_cr3;
    asm volatile ("movl %%cr2,%%eax" : "=a" (old_cr3));
    return old_cr3;
}

static inline uint32_t get_cr3(void)
{
    uint32_t old_cr3;
    asm volatile ("movl %%cr3,%%eax" : "=a" (old_cr3));
    return old_cr3;
}

static inline void set_cr3(uint32_t new_cr3)
{
    asm volatile ("movl %%eax,%%cr3" :: "a" (new_cr3));
}

// Create the initial page directory structure.
// (The actual page directory will already exist)
void create_init_pagedir(struct pagedir *init_pagedir);

// Create a page directory. The page directory entries for the kernel's
// address space are duplicated.
// Caller is expected to provide storage for the 'struct pagedir'.
// The address of the 'struct pagedir' must not change!
// Returns 0=ok, 1=error (out of memory)
int pagedir_create(struct pagedir *pagedir);

// Destroy a page directory
void pagedir_destroy(struct pagedir *pagedir);

// Return the physical address pointed to by 'virtual'
uintptr_t xlat_v2p(struct pagedir *pd, uintptr_t virtual);

// Alter the current memory map: map a physical region to the
// current virtual address space.
// Flags are PTE_* constants
// Return: 0=ok, 1=error
int map_mem(struct pagedir *pd, uintptr_t physical, uintptr_t virtual, size_t n_pages, int flags);

// Switch to a new page directory. Be careful. Paging is confusing.
void pagedir_switch(struct pagedir *new_pagedir);

#endif
