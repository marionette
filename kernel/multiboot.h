/*
 *    Copyright (c) 2008 Joshua Phillips. All rights reserved.
 *  
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are
 *  met:
 *  
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in
 *       the documentation and/or other materials provided with the
 *       distribution.
 *  
 *  THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 *  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 *  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 *  IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 *  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 *  IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef MULTIBOOT_H
#define MULTIBOOT_H

// magic numbers
#define MB_MBH_MAGIC 0x1BADB002 // magic number in multiboot header
#define MB_BOOT_MAGIC 0x2BADB002 // magic number in eax at startup

enum {
    MBI_MEM_XXX          = 1 << 0,  // 0x01
    MBI_BOOT_DEVICE      = 1 << 1,  // 0x02
    MBI_CMDLINE          = 1 << 2,  // 0x04
    MBI_MODS_XXX         = 1 << 3,  // 0x08
    MBI_AOUT_SYMS        = 1 << 4,  // 0x10
    MBI_ELF_SYMS         = 1 << 5,  // 0x20
    MBI_MMAP_XXX         = 1 << 6,  // 0x40
    MBI_DRIVES_XXX       = 1 << 7,  // 0x80
    MBI_CONFIG_TABLE     = 1 << 8,  // 0x100
    MBI_BOOT_LOADER_NAME = 1 << 9,  // 0x200
    MBI_APM_TABLE        = 1 << 10, // 0x400
    MBI_VBE_XXX          = 1 << 11, // 0x800
};

struct mb_info {
	unsigned long flags, mem_lower, mem_upper,
		boot_device, cmd_line, mods_count, mods_addr;
    union {
        struct {
            unsigned long syms[4];
        } aout;
        struct {
            unsigned long e_shnum, e_shentsize,
                          e_shaddr, e_shstrndx;
        } elf;
    } syms;

    unsigned long mmap_length, mmap_addr, drives_length,
		drives_addr, config_table, boot_loader_name,
		apm_table, vbe_control_info, vbe_mode_info,
		vbe_mode, vbe_interface_seg, vbe_interface_off,
		vbe_interface_len;
};

struct mb_module {
	unsigned long mod_start, mod_end, string, reserved;
};

struct mb_mmap {
	unsigned long size;
	unsigned long long base_addr, length;
	unsigned long type;
};

struct mb_drive {
	unsigned long size;
	unsigned char drive_number, drive_mode;
	unsigned short drive_cylinders;
	unsigned char drive_heads, drive_sectors;
	unsigned short drive_ports[1];
};

#endif

