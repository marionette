/*
 *    Copyright (c) 2008 Joshua Phillips. All rights reserved.
 *  
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are
 *  met:
 *  
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in
 *       the documentation and/or other materials provided with the
 *       distribution.
 *  
 *  THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 *  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 *  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 *  IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 *  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 *  IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef PORTIO_H
#define PORTIO_H

#include "stddef.h"

static inline void outb(int port, unsigned char value)
{
	asm volatile ("outb %%al,%%dx" :: "a" (value), "d" (port));
}

static inline void outw(int port, unsigned short value)
{
	asm volatile ("outw %%ax,%%dx" :: "a" (value), "d" (port));
}

static inline void outl(int port, unsigned long value)
{
	asm volatile ("outl %%eax,%%dx" :: "a" (value), "d" (port));
}

static inline unsigned char inb(int port)
{
	unsigned char value;
	asm volatile ("inb %%dx,%%al" : "=a" (value) : "d" (port));
	return value;
}

static inline unsigned short inw(int port)
{
	unsigned short value;
	asm volatile ("inw %%dx,%%ax" : "=a" (value) : "d" (port));
	return value;
}

static inline unsigned long inl(int port)
{
	unsigned long value;
	asm volatile ("inl %%dx,%%eax" : "=a" (value) : "d" (port));
	return value;
}

#endif

