/*
 *    Copyright (c) 2009 Joshua Phillips. All rights reserved.
 *  
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are
 *  met:
 *  
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in
 *       the documentation and/or other materials provided with the
 *       distribution.
 *  
 *  THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 *  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 *  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 *  IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 *  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 *  IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 *  Multiboot information reader
 */

#include "read-multiboot.h"

// Returns true if 'region' is in range
static bool is_moar_regions(const struct mb_info *mbi, const struct mb_mmap *region)
{
    return region < (const struct mb_mmap *) (mbi->mmap_addr + mbi->mmap_length);
}

bool read_mmap_start(const struct mb_info *mbi, const struct mb_mmap **p_region)
{
    if (mbi->flags & MBI_MMAP_XXX){
        *p_region = (const struct mb_mmap *) mbi->mmap_addr;
        return is_moar_regions(mbi, *p_region);
    } else {
        return false;
    }
}

bool read_mmap_continue(const struct mb_info *mbi, const struct mb_mmap **p_region)
{
    *p_region = (const struct mb_mmap *)
      ((const char *) *p_region + (*p_region)->size + 4);
    return is_moar_regions(mbi, *p_region);
}
