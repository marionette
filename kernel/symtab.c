/*
 *    Copyright (c) 2009 Joshua Phillips. All rights reserved.
 *  
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are
 *  met:
 *  
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in
 *       the documentation and/or other materials provided with the
 *       distribution.
 *  
 *  THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 *  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 *  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 *  IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 *  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 *  IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "symtab.h"
#include "trace.h"
#include "console.h"
#include "stdlib.h"
#include "string.h"

enum {
    MAX_HASH = 256,
};

struct sym {
    struct sym *hash_prev;
    uintptr_t value;
    char name[1];
};

static struct sym *hashtab[MAX_HASH];

static int hash(const char *str)
{
    unsigned int hash = 0;
    while (*str){
        hash = hash * 263 + *str;
        ++str;
    }
    return hash % MAX_HASH;
}

void symtab_init(void)
{
    memset(hashtab, 0, sizeof hashtab);
}

int symtab_insert(const char *name, uintptr_t value)
{
    const int h = hash(name);
    struct sym *s;

    s = hashtab[h];
    
    while (s){
        if (!strcmp(name, s->name)){
            // already in the symbol table
            return 0;
        }
        s = s->hash_prev;
    }

    // insert into hash table
    s = malloc(sizeof *s + strlen(name));
    if (!s){
        TRACE("memory full");
        return 1;
    }

    strcpy(s->name, name);
    s->value = value;
    s->hash_prev = hashtab[h];
    hashtab[h] = s;
    return 0;
}

uintptr_t symtab_lookup(const char *name)
{
    int h = hash(name);
    struct sym *s;

    s = hashtab[h];
    while (s){
        if (!strcmp(name, s->name)){
            return s->value;
        }
        s = s->hash_prev;
    }
    return 0;
}
