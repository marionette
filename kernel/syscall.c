/*
 *    Copyright (c) 2009 Joshua Phillips. All rights reserved.
 *  
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are
 *  met:
 *  
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in
 *       the documentation and/or other materials provided with the
 *       distribution.
 *  
 *  THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 *  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 *  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 *  IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 *  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 *  IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "syscall.h"
#include "string.h"
#include "interrupt.h"
#include "trace.h"
#include "exit.h"

static const syscall_t syscall_table[] = {
    NULL,
    sys_exit,
};

static const int n_syscalls = sizeof syscall_table / sizeof *syscall_table;

static void syscall_handler(int vector, struct interrupt_stack *is)
{
    int syscall_n = is->r.eax;
    struct syscall_args args;
    if (syscall_n < 0 || syscall_n >= n_syscalls
      || !syscall_table[syscall_n]){
        TRACE("Invalid syscall: %d", syscall_n);
        is->r.eax = -1;
        // TODO: set errno (HOW? Where?)
    } else {
        // get the system call arguments from the registers
        args.args[0].ui = is->r.ebx;
        args.args[1].ui = is->r.ecx;
        args.args[2].ui = is->r.edx;
        args.args[3].ui = is->r.esi;
        args.args[4].ui = is->r.edi;
        is->r.eax = syscall_table[syscall_n](&args);
        TRACE("Done syscall: returning to %.8X", is->eip);
    }
}

void syscall_init(void)
{
    set_interrupt(0x80, syscall_handler);
    interrupt_flags(0x80, INTR_USER);
}
