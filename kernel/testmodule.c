#define MODULE_NAME testmodule
#define MODULE
#include "module.h"
#include "console.h"

int bss_test;

MODULE_INIT(testmodule_init)
{
    bss_test = 0;
    console_puts(&tty1, "Hello! I'm a module!\n");
    return 0;
}
