/*
 *    Copyright (c) 2008 Joshua Phillips. All rights reserved.
 *  
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are
 *  met:
 *  
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in
 *       the documentation and/or other materials provided with the
 *       distribution.
 *  
 *  THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 *  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 *  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 *  IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 *  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 *  IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


#include "thread.h"
#include "stdlib.h"
#include "string.h"
#include "trace.h"
#include "panic.h"
#include "interrupt.h"
#include "timer.h"

struct thread {
    struct thread *prev, *next;
    struct pusha_struct regs;
    int suspend_count;
    int user_space;
    uint32_t eip, eflags;
    struct pagedir *pd;
};

static struct thread *thread_first = NULL;
static struct thread *thread_last = NULL;
static struct thread *thread_current = NULL;

int preempt_on = 0;
tick_t next_preempt = 0;

// defined in thread-asm.S
void context_switch(struct pusha_struct *regs, uint32_t eip, uint32_t eflags);
void context_switch_user(struct pusha_struct *regs, uint32_t eip, uint32_t eflags);
void store_call(void (* func_ptr)(void));

void thread_save_call_state(uint32_t ebx, uint32_t esp, uint32_t ebp, uint32_t esi, uint32_t edi, uint32_t eip, uint32_t eflags)
{
    if (!thread_current){
        return;
    }
    thread_current->regs.eax = 0;
    thread_current->regs.ecx = 0;
    thread_current->regs.edx = 0;
    thread_current->regs.ebx = ebx;
    thread_current->regs.esp = esp;
    thread_current->regs.ebp = ebp;
    thread_current->regs.esi = esi;
    thread_current->regs.edi = edi;
    thread_current->eip = eip;
    thread_current->eflags = eflags;
}

static struct thread *get_next_thread(void)
{
    struct thread *t;
    if (!thread_current){
        t = thread_last;
    } else {
        t = thread_current;
    }
    if (!t){
        return NULL;
    }
    for (;;){
        if (t->next){
            t = t->next;
        } else {
            t = thread_first;
        }
        if (t->suspend_count == 0){
            return t;
        }
    }
}

static void switch_to_thread(struct thread *thread)
{
    thread_current = thread;
    preempt_on = 1;
    next_preempt = get_tick() + 100;
    if (thread->pd){
        pagedir_switch(thread->pd);
    }
    if (thread->user_space){
        context_switch_user(&thread->regs, thread->eip, thread->eflags);
    } else {
        context_switch(&thread->regs, thread->eip, thread->eflags);
    }
}

// initialize the threading system
void thread_sys_init(void)
{
    
}

// start the threading system
void thread_sys_start(void)
{
    struct thread *next_thread;
    next_thread = get_next_thread();
    if (next_thread){
        switch_to_thread(next_thread);
    } else {
        TRACE("thread_sys_start: no threads");
    }
}

struct thread *thread_new(void)
{
    struct thread *new_thread;
    new_thread = malloc(sizeof *new_thread);
    if (!new_thread){
        return NULL;
    }

    // set thread as suspended
    new_thread->suspend_count = 1;

    // linked-list insert
    new_thread->prev = thread_last;
    new_thread->next = NULL;
    if (thread_last){
        thread_last->next = new_thread;
    } else {
        thread_first = new_thread;
    }
    thread_last = new_thread;

    memset(&new_thread->regs, 0, sizeof new_thread->regs);
    new_thread->eip = 0;
    new_thread->eflags = 0x0202;
    new_thread->pd = NULL;

    return new_thread;
}

struct thread *thread_get_current(void)
{
    return thread_current;
}

void thread_set_pagedir(struct thread *t, struct pagedir *pd)
{
    t->pd = pd;
}

void thread_set_eip(struct thread *t, uint32_t eip)
{
    t->eip = eip;
}

void thread_set_esp(struct thread *t, uint32_t esp)
{
    t->regs.esp = esp;
}

void thread_set_userspace(struct thread *t, int is_userspace)
{
    t->user_space = !!is_userspace;
}

void thread_resume(struct thread *t)
{
    if (t->suspend_count > 0){
        t->suspend_count--;
    }
}

void thread_suspend(struct thread *t)
{
    t->suspend_count++;
    // TODO: if this is the current thread,
    // need to switch away from it.
}

static void next_switch(void)
{
    struct thread *next_thread;
    next_thread = get_next_thread();
    if (next_thread){
        switch_to_thread(next_thread);
    } else {
        panic("no threads");
    }
}

void thread_delete(struct thread *t)
{
    struct thread *thread_next = t->next;

    // remove from list
    *(t->prev ? &t->prev->next : &thread_first)
      = t->next;
    *(t->next ? &t->next->prev : &thread_last)
      = t->prev;
    
    // clean up the thread
    // TODO; more cleanup
    free(t);

    // change from the thread, if it's current
    if (t == thread_current){
        thread_current = thread_next;
        next_switch();
    }
}

void yield_internal(void)
{
    struct thread *next_thread;
    next_thread = get_next_thread();
    if (next_thread){
        switch_to_thread(next_thread);
    } else {
        TRACE("yield_internal: no threads");
    }
}

void yield(void)
{
    store_call(yield_internal);
}

void preempt(struct interrupt_stack *is)
{
    struct thread *next_thread;
    preempt_on = 0;
    if (thread_current){
        // store thread state
        thread_current->regs = is->r;
        thread_current->eip = is->eip;
        thread_current->eflags = is->eflags;
    }
    next_thread = get_next_thread();
    if (next_thread){
        switch_to_thread(next_thread);
    } else {
        TRACE("preempt: no threads");
    }
}
