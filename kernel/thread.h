/*
 *    Copyright (c) 2008 Joshua Phillips. All rights reserved.
 *  
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are
 *  met:
 *  
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in
 *       the documentation and/or other materials provided with the
 *       distribution.
 *  
 *  THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 *  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 *  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 *  IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 *  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 *  IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef THREAD_H
#define THREAD_H

#include "cpustruct.h"
#include "stdint.h"
#include "timer.h"
#include "interrupt.h"
#include "mm/paging.h"

struct thread;

// only for use by timer.c
extern int preempt_on;
extern tick_t next_preempt;

void thread_sys_init(void); // initialize the threading system
void thread_sys_start(void); // start the threading system
struct thread *thread_new(void);
struct thread *thread_get_current(void);
void thread_set_pagedir(struct thread *t, struct pagedir *pd);
void thread_set_eip(struct thread *t, uint32_t eip);
void thread_set_esp(struct thread *t, uint32_t esp);
void thread_set_userspace(struct thread *t, int is_userspace);
void thread_resume(struct thread *t);
void thread_suspend(struct thread *t);
void thread_delete(struct thread *t);
void yield_internal(void);
void yield(void);
void preempt(struct interrupt_stack *is);

#endif
