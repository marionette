/*
 *    Copyright (c) 2008 Joshua Phillips. All rights reserved.
 *  
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are
 *  met:
 *  
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in
 *       the documentation and/or other materials provided with the
 *       distribution.
 *  
 *  THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 *  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 *  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 *  IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 *  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 *  IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "timer.h"
#include "portio.h"
#include "console.h"
#include "interrupt.h"
#include "thread.h"

#define TIMER_IRQ 0
#define TIMER_FREQ 1000
#define CTL 0x43
#define CNT0 0x40
#define CNT1 0x41
#define CNT2 0x42

static tick_t tick = 0; // current tick number
static unsigned long freq; // current timer frequency

tick_t get_tick(void)
{
	return tick;
}

static void timer_interrupt(int vector, struct interrupt_stack *is)
{
	tick++;
    if (preempt_on && (next_preempt <= tick)){
        preempt(is);
    }
	ack_irq(TIMER_IRQ);
}

static void set_frequency(unsigned long new_freq)
{
	unsigned short divisor;
	divisor = 1193180 / new_freq;
	freq = (1193180LL << 32) / divisor;
	outb(CTL, 0x36);
	outb(CNT0, divisor);
	outb(CNT0, divisor >> 8);
}

void timer_sys_start(void)
{
	unsigned char p61h;
	// Timer's frequency is 1,193,180 Hz
	outb(CTL, 0x54);
	outb(CNT1, 18); // LSB only clock divisor (?)
	tick = 0;
	set_interrupt(irq_to_int(TIMER_IRQ), timer_interrupt);
	set_frequency(TIMER_FREQ);
	unmask_irq(TIMER_IRQ);
	
	outb(CTL, 0xB6);
	outb(CNT2, 54);
	outb(CNT2, 124);
	p61h = inb(0x61);
	p61h |= 3;
	outb(0x61, p61h);
}

