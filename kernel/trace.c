/*
 *    Copyright (c) 2009 Joshua Phillips. All rights reserved.
 *  
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are
 *  met:
 *  
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in
 *       the documentation and/or other materials provided with the
 *       distribution.
 *  
 *  THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 *  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 *  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 *  IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 *  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 *  IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 *   The TRACE macro provides simple debugging output, either to the
 *   serial port or vgatext.
 */

#include "trace.h"
#include "serial.h"
#include "console.h"
#include "stdio.h"
#include "string.h"
#include "portio.h"

// Use a hack in Bochs - repeatedly output to port 0xE9
// On real hardware, 0xE9 is unused, so it does nothing

static void bochs_e9_hack(const char *str)
{
    while (*str){
        outb(0xE9, *str);
        ++str;
    }
}

void trace_internal(const char *file, int line, const char *func, const char *fmt, ...)
{
    char str[256], *short_str, *actual_msg, c;
    int len;
    va_list ap;

    // file:line: (function): message
    // ^str       ^short_str  ^actual_msg

    va_start(ap, fmt);

    len = snprintf(str, 256, "%s:%d: ", file, line);
    short_str = str + len;
    len += snprintf(str + len, 256 - len, "(%s): ", func);
    actual_msg = str + len;
    len += vsnprintf(str + len, 256 - len, fmt, ap);
    strncat(str + len, "\n", 256 - len);
    str[255] = '\0';
    if (serial_is_initialized(&com1)){
        serial_send(&com1, str);
    }
    bochs_e9_hack(str);

    // print "(function): message" to console
    // with colours
    c = *actual_msg;
    *actual_msg = '\0';
    console_colour_fg(&tty1, COLOUR_BRIGHT_WHITE);
    console_puts(&tty1, short_str);
    console_colour_default(&tty1);
    *actual_msg = c;
    console_puts(&tty1, actual_msg);

    va_end(ap);
}
