/*
 *    Copyright (c) 2008 Joshua Phillips. All rights reserved.
 *  
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are
 *  met:
 *  
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in
 *       the documentation and/or other materials provided with the
 *       distribution.
 *  
 *  THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 *  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 *  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 *  IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 *  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 *  IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef _EXTLIB_H
#define _EXTLIB_H

// return a/b rounded up
static inline unsigned long uldivru(unsigned long a, unsigned long b)
{
	unsigned long d;
	d = a / b;
	if (a % b)
		d++;
	return d;
}

static inline unsigned long long ulldivru(unsigned long long a, unsigned long long b)
{
	unsigned long long d;
	d = a / b;
	if (a % b)
		d++;
	return d;
}

int log2ll(long long n); // return base-2 log of n, rounded down

// return log base 2 of v
static inline unsigned int ilog2(unsigned int v)
{
	static const unsigned int b[] = {0x2, 0xC, 0xF0, 0xFF00, 0xFFFF0000};
	static const unsigned int S[] = {1, 2, 4, 8, 16};
	int i;
	register unsigned int r = 0; // result goes here
	for (i=4; i>=0; i--){
		if (v & b[i]){
			v >>= S[i];
			r |= S[i];
		}
	}
	return r;
}

// return log base 2 of v, rounded up
static inline unsigned int ilog2up(unsigned int v)
{
	// do it the nasty way
	unsigned int log = ilog2(v);
	if (v & ((1 << log) - 1))
		log++;
	return log;
}

// return 'value' rounded up to nearest 'alignment' multiple
// hopefully, since these are inline, cc will optimize the division
// and modulus for shifts and ands.
static inline unsigned long alignup(unsigned long value, unsigned long alignment)
{
	return uldivru(value, alignment) * alignment;
}

// return 'value' rounded down to nearest 'alignment' multiple
static inline unsigned long aligndn(unsigned long value, unsigned long alignment)
{
	return (value / alignment) * alignment;
}

static inline unsigned int bsf(unsigned int x)
{
	unsigned int result;
	asm volatile (
	  "bsfl %1,%0" : "=r" (result) : "r" (x));
	return result;
}

#endif

