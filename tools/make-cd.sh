#!/usr/bin/env sh
# Make a Marionette CD image

KERNEL=build/kernel/kernel.elf
MODS="build/kernel/testmodule.ko"
CDDIR=cdrom
GRUBDIR=/usr/lib/grub/i386-pc
OUTPUT=cdrom.iso

# create directory if it doesn't exist - die on failure
mkdir_e(){
    [ -d "$1" ] || mkdir -v "$1" || die "failed to create directory $1"
}

die(){
    echo $*
    exit 1
}

[ "$1" == "--help" ] && {
    echo "Usage: $0"
    echo "(This command takes no arguments)"
    echo ""
    echo "Creates a bootable CD image for Marionette."
    echo "Requires mkisofs and GRUB"
    echo ""
    echo "Please edit this script to alter the settings."
    echo ""
    echo "CD-ROM directory: $CDDIR"
    echo "GRUB files in: $GRUBDIR"
    echo "Kernel image: $KERNEL"
    echo "Output ISO image: $OUTPUT"
    exit 0
}

mkdir_e "$CDDIR"
mkdir_e "$CDDIR/boot"
mkdir_e "$CDDIR/boot/grub"

if [ ! -f "$CDDIR/boot/grub/stage2_eltorito" ]
then
    STAGE2="$GRUBDIR/stage2_eltorito"
    if [ -f "$STAGE2" -a -r "$STAGE2" ]
    then
        cp -v "$GRUBDIR/stage2_eltorito" "$CDDIR/boot/grub/" || die "failed to copy $STAGE2"
    else
        # try to download it
        echo "$STAGE2 does not exist, is not a file, or is not readable. Attempting to download it."
        wget "http://xlq-experiment.com/marionette/res/stage2_eltorito" -O "$CDDIR/boot/grub/stage2_eltorito" \
            || die "$STAGE2 does not exist, and I couldn't download it."
    fi
fi

# install a file into the CD image
insfile(){
    [ -f "$1" ] || die "$1 does not exist. Maybe you forgot to build it?"
    cp -v "$1" "$CDDIR/boot/" || die "could not install $1"
}

insfile "$KERNEL"

for i in $MODS
do
    insfile "$i"
done

cat > "$CDDIR/boot/grub/menu.lst" << EOF
default 0
timeout 3

title Marionette
kernel /boot/kernel.elf
module /boot/testmodule.ko

title Boot from hard disk
chainloader (hd0)+1
EOF

mkisofs -R -b boot/grub/stage2_eltorito -no-emul-boot -boot-load-size 4 \
        -boot-info-table -A "Marionette CD" -o "$OUTPUT" "$CDDIR" \
        || die "failed to create the CD image"

echo "Your CD image ($OUTPUT) awaits."
